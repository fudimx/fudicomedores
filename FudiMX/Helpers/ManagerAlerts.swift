//
//  ManagerAlerts.swift
//  FudiMX
//
//  Created by Cristian Canedo on 20/05/22.
//

import Foundation
import UIKit

class ManagerAlerts{
    
    static func simpleAlert(on ctx:UIViewController ,title:String, description:String, action:@escaping() -> Void?, actionName:String? = "Cancel"){
        
        let alert = UIAlertController(title: title, message: description, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: actionName, style: .default, handler: {res in
            action()
        }))
        ctx.present(alert, animated: true, completion: nil)
    }
}
