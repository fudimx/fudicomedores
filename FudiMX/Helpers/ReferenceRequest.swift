//
//  ReferenceRequest.swift
//  FudiMX
//
//  Created by Cristian Canedo on 06/04/22.
//

import Foundation

enum Referencerequest{
    case getBanner
    case getBranch
    case getLocations
    case getLocationsRequest
    case getMenu
    case getFolioOrder
    case createOrder
    case createLocalOrder
    case getAddOns
    case setAddOns
    case setSizes
    case getMenuRequest
    case getProducts
    case saveBanner
    case saveBranch
    case getBranchesRequest
    case saveLocation
    case saveMenu
    case saveShoppingCart
    case getShoppingCart
    case deleteShoppingCart
    case deleteItemCart
    case haveShoppingCart
    case saveOrder
    case getOrder
    
    case errorResponse
}

enum TypeAlert{
    case Success
    case Error
    case Warning
    case Info
}
