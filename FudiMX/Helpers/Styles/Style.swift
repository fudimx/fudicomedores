//
//  Style.swift
//  FudiMX
//
//  Created by Cristian Canedo on 26/04/22.
//

import Foundation
import UIKit

enum color: String{
    case PrimaryColor
}

func setColor(_ color : color) -> UIColor {
    return UIColor.init(named: color.rawValue)!;
}
