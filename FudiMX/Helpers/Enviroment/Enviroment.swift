//
//  Enviroment.swift
//  FudiMX
//
//  Created by Cristian Canedo on 22/04/22.
//

import Foundation
import UIKit

enum Environment: String {
    case Gral = "gral"
    case Dev = "dev"
    
    private var dictionary:NSDictionary? {
        guard let path = Bundle.main.path(forResource: "Constants", ofType: "plist"), let dictionary = NSDictionary(contentsOfFile: path) else {
            return nil
        }
        return dictionary
    }

    var baseURL: String {
        guard let baseURLDictionary = self.dictionary else {return ""}
        if let urls = baseURLDictionary.object(forKey: "baseURL") as? NSDictionary {
            switch self {
            case .Gral: return urls.object(forKey: "gral") as! String
            case .Dev: return urls.object(forKey: "dev") as! String
            }
        } else{
            return ""
        }
    }

}
