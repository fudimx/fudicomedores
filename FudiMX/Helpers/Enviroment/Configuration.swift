//
//  Configuration.swift
//  FudiMX
//
//  Created by Cristian Canedo on 22/04/22.
//

import UIKit

struct Configuration {
    lazy var environment: Environment = {
        if let configuration = Bundle.main.object(forInfoDictionaryKey: "Configuration") as? String {
            if configuration.range(of: "Dev") != nil {
                return Environment.Dev
            }else{
                return Environment.Gral
            }
        }
        return Environment.Gral
    }()
    
}
