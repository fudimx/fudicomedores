//
//  OrderItemModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 21/04/22.
//

import Foundation

struct OrderItemModel:Codable{
    var id:String?
    var idOrder:String?
    var idProduct:String
    var idShoppingCart:String
    var notes:String?
    var quantity:Int?
    var total:Double?
    
    var addOns:[String]
    var sizes:[SizeCartModel]
}
