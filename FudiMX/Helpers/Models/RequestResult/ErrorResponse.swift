//
//  LocationResponseModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 22/04/22.
//

import Foundation

struct ErrorResponse{
    var message:String?
    var codeError:Int?
}
