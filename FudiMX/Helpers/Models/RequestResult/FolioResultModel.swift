//
//  FolioResultModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 08/05/22.
//

import Foundation

struct FolioResultModel:Codable{
    var folioPedido:String?
    var cMensaje:String?
    var bError:Bool?
}
