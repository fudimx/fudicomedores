//
//  BaseObjectModel.swift
//  Administrate
//
//  Created by Cristian Canedo on 12/01/22.
//

import Foundation
struct BaseObjectModel<T:Codable>:Codable{
    var bError:Bool
    var data:T?
    var cache:Bool?
}
