//
//  AddOnsModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 26/04/22.
//

import Foundation

struct AddOnsModel:Codable{
    var complementos:[String]
    var pasoPreparacion:String
    var complementosSelect:[Bool]? = []
}
