//
//  CreateOrderResponse.swift
//  FudiMX
//
//  Created by Cristian Canedo on 29/06/22.
//

import Foundation

struct CreateOrderResponse:Codable{
    var bError:Bool
    var cMensaje:String
    var _id:String
    var pedido:OrderModelResponse
}
struct OrderModelResponse:Codable{
    var _id:String
    var bPedidoGuardado:Bool
    var pedido:SecondOrderModelResponse
    var status:Int
    var fechaPedido:String
    var nFechaPedido:Int
    var nSemanaPedido:Int
    var bPedidoInvitado:Bool
    var bCompraPicnic:Bool
    var grupo:String
    var nIdRestaurante:String
    var nIdSucursal:String
    var nIdCategoria:Int
    var regionEntrega:String
    var nDiaPedido:Int
    var nDiaSemanaPedido:Int
    var nMesPedido:Int
    var nAnioPedido:Int
}

struct SecondOrderModelResponse:Codable{
    var formaDePago:MethodPayModelResponse
    var lstPromociones:[String]
    var total:Double
    var regionEtrega:String
    var nIdRestaurante:String
    var index:Int
    var fechaPedido:String
    var lstBebidas:[String]
    var nIdCupon:String
    var nIdCategoria:Int
    var nombreSucursal:String
    var bRecoger:Bool
    var playerId:String
    var costoEnvio:Double
    var nIdSucursal:String
    var statusPedido:Int
    var nombreRestaurante:String
    var tipoCompra:Int
    var lstComplementos:[String]
    var bCompraInvitado:Bool
    var subTotal:Double
    var lstPlatillos:[DishOrderModelResponse]
    
    
}
struct MethodPayModelResponse:Codable{
    var pagaCon:Int
    var nTipoPago:Int
}

struct DishOrderModelResponse:Codable{
    var _id:String
    var bFoto:Bool
    var bIngredienteElegir:Bool
    var bPlatilloHome:Bool
    var cDescripcion:String
    var cFoto:String
    var cNombrePlatillo:String
    var comentarios:String
    var lstComplementos:[String]
    var lstIngredientes:[String]
    var lstPreparacion:[AddOnsModel]
    var lstTamanios:[SizeCartModel]
    var nIdCategoria:String
    var nIdRestaurante:String
    var nIngredientes:Int
    var nPrecio:Int
    var preparacion:[String]
    var quantity:Int
}
