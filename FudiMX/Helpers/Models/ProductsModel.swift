//
//  ProductsModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 22/04/22.
//

import Foundation

struct ProductsModel:Codable{
    var _id:String
    var nIdCategoria:String
    var nIdRestaurante: String
    var cNombrePlatillo:String
    var cDescripcion: String?
    var cFoto:String
    var bActivo:Bool
    var bDisponible:Bool
    var Categoria:String
    var lstTamanios:[SizeModel] = []
    var lstPreparacion:[AddOnsModel]? = []
}


