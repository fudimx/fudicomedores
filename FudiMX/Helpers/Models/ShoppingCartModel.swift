//
//  ShoppingCartModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 19/04/22.
//

import Foundation

struct ShoppingCartModel:Codable{
    var id:String?
    var idProduct:String?
    var notes:String?
    var quantity:Int?
    var total:Double?
    var nameProduct:String?
    var status:Bool?
    var addOns: [String]
    var sizes:[SizeCartModel]
    
    init(id value:String,idProduct value1:String,notes value2:String,quantity value3:Int, total value4:Double, nameProduct value5:String, status value6:Bool, addOns:[String], sizes:[SizeCartModel]){
        self.id = value
        self.idProduct = value1
        self.notes = value2
        self.quantity = value3
        self.total = value4
        self.nameProduct = value5
        self.status = value6
        self.addOns = addOns
        self.sizes = sizes
    }
}
