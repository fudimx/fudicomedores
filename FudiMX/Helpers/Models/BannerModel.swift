//
//  BannerDtoModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 10/04/22.
//

import Foundation

struct BannerModel:Codable{
    var id:String?
    var image:String?
    var idLocation:String?
}
