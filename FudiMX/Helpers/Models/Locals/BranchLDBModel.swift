//
//  BranchLDBModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 11/04/22.
//

import Foundation
import CoreData


@objc(BranchLDBModel)
class BranchLDBModel: NSManagedObject{
    @NSManaged var id:String?
    @NSManaged var nIdRestaurante: String
    @NSManaged var cNombreSucursal: String
    @NSManaged var cDireccion: String
    @NSManaged var dHoraApertura: String
    @NSManaged var nTiempoEntrega: NSNumber
    @NSManaged var dHoraCierre:String
    @NSManaged var bActivo:Bool
    @NSManaged var nCostoEntrega:NSNumber
    @NSManaged var nTiempoEspera:NSNumber
    @NSManaged var cTelefono:String
    @NSManaged var tipoEntrega:NSNumber
    @NSManaged var nHoraApertura:NSNumber
    @NSManaged var nMinutoApertura:NSNumber
    @NSManaged var nHoraCierre:NSNumber
    @NSManaged var nMinutoCierre:NSNumber
    @NSManaged var time_zone:String
    @NSManaged var bSoloRecoger:Bool
    @NSManaged var nLatitud:NSNumber
    @NSManaged var nLongitud:NSNumber
    @NSManaged var regionEntrega:String
    @NSManaged var cNombreRestaurante:String
    @NSManaged var nIdCategoria:NSNumber
    @NSManaged var barraBebidas:Bool
    @NSManaged var nIdFormaPago:NSNumber
    @NSManaged var pedidoMinimo:NSNumber
    @NSManaged var nTipoDescuento:NSNumber
    @NSManaged var descuentoPicnic:NSNumber
}
