//
//  ProductsLDBModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 16/04/22.
//

import Foundation
import CoreData

@objc(ProductsLDBModel)
class ProductsLDBModel:NSManagedObject{
    @NSManaged var id:String?
    @NSManaged var nIdCategoria:String?
    @NSManaged var nIdRestaurante: String?
    @NSManaged var cNombrePlatillo:String?
    @NSManaged var cDescripcion: String?
    @NSManaged var cFoto:String?
    @NSManaged var bActivo:Bool
    @NSManaged var bDisponible:Bool
    @NSManaged var categoria:String?
}
