//
//  SizeLDBModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 26/04/22.
//

import Foundation
import CoreData

@objc(ProductsSizeLDBModel)
class ProductsSizeLDBModel:NSManagedObject{
    @NSManaged var cNombreTamanio:String
    @NSManaged var cDescripcion:String
    @NSManaged var nPrecio:Double
    @NSManaged var idProduct:String
    @NSManaged var bSeleccionado:Bool
}
