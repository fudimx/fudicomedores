//
//  AddOnsLDBModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 26/04/22.
//

import Foundation
import CoreData

@objc(AddOnsLDBModel)
class AddOnsLDBModel:NSManagedObject{
    @NSManaged var complementos:Data
    @NSManaged var pasoPreparacion:String
    @NSManaged var idProduct:String
}
