//
//  MenuLDBModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 15/04/22.
//

import Foundation
import CoreData

@objc(MenuLDBModel)
class MenuLDBModel:NSManagedObject{
    @NSManaged var id:String?
    @NSManaged var nIdRestaurante:String?
    @NSManaged var cDescripcion:String?
    @NSManaged var bActivo:Bool
}
