//
//  ShoppinggCartLDBModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 19/04/22.
//

import Foundation
import CoreData

@objc(ShoppingCartLDBModel)
class ShoppingCartLDBModel:NSManagedObject{
    @NSManaged var id:String?
    @NSManaged var idProduct:String?
    @NSManaged var notes:String?
    @NSManaged var quantity: NSNumber?
    @NSManaged var total: NSNumber?
    @NSManaged var status:Bool
}
