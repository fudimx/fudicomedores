//
//  AddOnsCartLDBModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 08/05/22.
//

import Foundation
import CoreData


@objc(SizeCartLDBModel)
class SizeCartLDBModel:NSManagedObject{
    @NSManaged var bSeleccionado:Bool
    @NSManaged var cNombreTamanio:String?
    @NSManaged var nPrecio:NSNumber?
    @NSManaged var quantity:NSNumber?
    @NSManaged var idShoppingCart:String
}
