//
//  BannerLDBModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 10/04/22.
//

import Foundation
import CoreData


@objc(BannerLDBModel)
class BannerLDBModel: NSManagedObject{
    @NSManaged var id:String?
    @NSManaged var idLocation: String?
    @NSManaged var image: String?
}
