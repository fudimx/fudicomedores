//
//  OrderLDBModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 21/04/22.
//

import Foundation
import CoreData

@objc(OrderLDBModel)
class OrderLDBModel:NSManagedObject{
    @NSManaged var id:String?
    @NSManaged var date:Date?
    @NSManaged var idBranch:String?
    @NSManaged var idLocation:String?
    @NSManaged var idMethodPay:String?
    @NSManaged var status:String?
    @NSManaged var total: NSNumber?
}
