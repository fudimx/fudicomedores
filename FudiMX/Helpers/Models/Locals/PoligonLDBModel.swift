//
//  PoligonModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 22/04/22.
//

import Foundation
import CoreData

@objc(PoligonLDBModel)
class PoligonLDBModel:NSManagedObject{
    @NSManaged var lat:NSNumber
    @NSManaged var lng:NSNumber
    @NSManaged var idLocation:String
}
