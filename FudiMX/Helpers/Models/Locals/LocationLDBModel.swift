//
//  LocationLDBModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 14/04/22.
//

import Foundation
import CoreData

@objc(LocationLDBModel)
public class LocationLDBModel: NSManagedObject {
    @NSManaged var id:String
    @NSManaged var nombreRegion:String
    @NSManaged var plazaComercial:Bool
    @NSManaged var ubicacion:String
}
