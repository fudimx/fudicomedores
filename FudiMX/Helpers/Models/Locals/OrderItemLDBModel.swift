//
//  OrderItemLDBModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 21/04/22.
//

import Foundation
import CoreData

@objc(OrderItemLDBModel)
class OrderItemLDBModel:NSManagedObject{
    @NSManaged var id:String
    @NSManaged var idOrder:String
    @NSManaged var idProduct:String
    @NSManaged var idShoppingCart:String
    @NSManaged var notes:String
    @NSManaged var quantity:NSNumber
    @NSManaged var total:NSNumber
}
