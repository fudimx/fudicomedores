//
//  AddOnsCartModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 08/05/22.
//

import Foundation

struct SizeCartModel:Codable{
     var bSeleccionado:Bool
     var cNombreTamanio:String?
     var nPrecio:Double?
    var quantity:Int?
    var idShoppingCart:String?
}
