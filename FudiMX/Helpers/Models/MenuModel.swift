//
//  MenuModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 22/04/22.
//

import Foundation

struct MenuModel:Codable{
    var _id:String
    var nIdRestaurante:String
    var cDescripcion:String?
    var bActivo:Bool
    var isSelected:Bool?
    var lstPlatillos:[ProductsModel]
    
    init(_ id:String, bActivo:Bool,_ isSelected:Bool,_ array:[ProductsModel],description:String,_ idBranch:String){
        self._id = id
        self.nIdRestaurante = idBranch
        self.cDescripcion = description
        self.bActivo = bActivo
        self.isSelected = isSelected
        self.lstPlatillos = array
    }
}

