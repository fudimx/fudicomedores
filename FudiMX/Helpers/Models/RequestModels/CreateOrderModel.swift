//
//  CreateOrderModelswift
//  FudiMX
//
//  Created by Cristian Canedo on 07/05/22
//

import Foundation

struct CreateOrderModel:Codable{
    var folioPedido:String?
    var pedido:PedidoModel?
}

struct PedidoModel:Codable{
    var bCompraInvitado:Bool?
    var bRecoger: Bool?
    var costoEnvio: Double?
    var fechaPedido: String?
    var formaDePago: MethodPayModel?
    var index: Int?
    var lstBebidas: [BebidasModel]?
    var lstComplementos: [ComplementosModel]?
    var lstPlatillos: [PlatillosModel]?
    var lstPromociones: [PromocionesModel]?
    var nIdCategoria: Int?
    var nIdCupon:String?
    var nIdRestaurante: String?
    var nIdSucursal: String?
    var nombreRestaurante: String?
    var nombreSucursal:String?
    var playerId:String?
    var regionEtrega: String?
    var statusPedido: Int?
    var subTotal: Double?
    var tipoCompra: Int?
    var total: Double?
    var usuario:UserPedidoModel?
}

struct UserPedidoModel:Codable{
    var cNombres:String
    var cTelefono:String
}
struct MethodPayModel:Codable{
    var nTipoPago:Int
    var pagaCon:Double
}
struct PlatillosModel:Codable{
    var _id:String
    var bFoto: Bool
    var bIngredienteElegir: Bool
    var bPlatilloHome: Bool
    var cDescripcion: String
    var cFoto:String
    var cNombrePlatillo: String
    var comentarios:String
    var lstComplementos: [ComplementosModel]
    var lstIngredientes: [String]
    var lstPreparacion: [AddOnsModel]
    var lstTamanios: [SizeCartModel]
    var nIdCategoria: String
    var nIdRestaurante: String
    var nIngredientes: Int
    var nPrecio: Double
    var preparacion: [String]
    var quantity: Int
}
struct PromocionesModel:Codable{
    
}
struct BebidasModel:Codable{
    
}
struct ComplementosModel:Codable{
    
}
