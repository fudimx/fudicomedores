//
//  WatchCartItems.swift
//  FudiMX
//
//  Created by Cristian Canedo on 20/04/22.
//

import Foundation

struct WatchCartItems:Codable{
    var totalPay:Double?
    var totalItems:Int?
}
