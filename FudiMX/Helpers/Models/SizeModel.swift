//
//  SizeModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 26/04/22.
//

import Foundation

struct SizeModel:Codable{
    var cNombreTamanio:String
    var cDescripcion:String?
    var nPrecio:Double
    var quantity:Int?
    var bSeleccionado:Bool?
}
