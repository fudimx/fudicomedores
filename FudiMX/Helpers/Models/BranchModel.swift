//
//  BranchModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 11/04/22.
//

import Foundation

struct BranchModel:Codable{
    var _id:String
    var nIdRestaurante: String
    var cNombreSucursal: String
    var cDireccion: String
    var dHoraApertura: String
    var nTiempoEntrega: Int
    var dHoraCierre:String
    var bActivo:Bool
    var nCostoEntrega:Int
    var nTiempoEspera:Int
    var cTelefono:String
    var tipoEntrega:Int
    var nHoraApertura:Int
    var nMinutoApertura:Int
    var nHoraCierre:Int
    var nMinutoCierre:Int
    var TIME_ZONE:String
    var bSoloRecoger:Bool
    var nLatitud:Double
    var nLongitud:Double
    var regionEntrega:String
    var cNombreRestaurante:String
    var nIdCategoria:Int
    var nIdFormaPago:Int
    var pedidoMinimo:Int?
    var nTipoDescuento:Int
    var descuentoPicnic:Int
    var barraBebidas:Bool?
    var diasDescuentoPicnic:[DiasDescuentoPicnic]
    var lstRangoEntrega:[PoligonoRegion]
}

struct DiasDescuentoPicnic:Codable{
    var numeroDia:Int
    var bActivo:Bool
    var dia:String
}

