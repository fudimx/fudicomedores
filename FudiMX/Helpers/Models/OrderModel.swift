//
//  OrderModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 21/04/22.
//

import Foundation

struct OrderModel:Codable{
    var id:String?
    var date:Date?
    var idBranch:String?
    var idLocation:String?
    var idMethodPay:String?
    var status:String?
    var total:Double?
    var items:[OrderItemModel] = []
}

struct RequestBaseOrder:Codable{
    var folioPedido:String
    var pedido:String
}

struct createOrder:Codable{
    var bCompraInvitado:Bool
    var bRecoger:Bool
    var costoEnvio:Double
    var fechaPedido:String
    var formaDePago:methodPay
    var index:Int
    var lstBebidas:[String]
    var lstComplementos:[String]
    var lstPlatillos:[PlatilloItem]
    var lstPromociones:[String]
    var nIdCategoria:Int
    var nIdCupon:String
    var playerId:String
    var nIdRestaurante:String
    var nIdSucursal:String
    var nombreRestaurante:String
    var nombreSucursal:String
    var regionEtrega:String
    var statusPedido:Int
    var subTotal:Double
    var tipoCompra:Int
    var total:Double
}
struct methodPay:Codable{
    var nTipoPago:Int
    var pagaCon:Double
}

struct PlatilloItem:Codable{
    var _id:String
    var bFoto:Bool
    var bIngredienteElegir:Bool
    var bPlatilloHome:Bool
    var cDescripcion:String
    var cFoto:String
    var cNombrePlatillo:String
    var comentarios:String
    var lstComplementos:[String]
    var lstTamanios:[SizeModel]
    var lstPreparacion:[AddOnsModel]
    var nIdCategoria:String
    var nIdRestaurante:String
    var nIngredientes:Int
    var nPrecio:Double
    var preparacion:String
    var quantity:Int
}
