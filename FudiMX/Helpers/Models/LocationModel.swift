//
//  LocationModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 14/04/22.
//

import Foundation

struct LocationModel:Codable{
    var poligonoRegion:[PoligonoRegion]
    var _id:String
    var nombreRegion:String
    var plazaComercial:Bool
    var ubicacion:String
}
struct PoligonoRegion:Codable{
    var lat:Double
    var lng:Double
}
