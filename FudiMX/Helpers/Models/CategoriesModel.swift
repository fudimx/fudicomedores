//
//  CategoriesModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 23/04/22.
//

import Foundation

struct CategoriesModel:Codable{
    
    var lstCategorias:[MenuModel] = []
    var _id:String?
    var nIdRestaurante:String?
}
