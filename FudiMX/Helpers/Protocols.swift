//
//  Protocols.swift
//  FudiMX
//
//  Created by Cristian Canedo on 09/04/22.
//

import Foundation

protocol SearchControllerDelegate{
    func onItemSelected(item:ProductsModel?)
}

protocol WelcomeControllerDelegate{
    func onLocationSelected(item:LocationModel?)
}
protocol HomeDelegate{
    func onCheckoutSuccess()
}

protocol ProductDetailsDelegate{
    func onWriteNote(note:String)
    func onAddOnsSelected(addOns:[String])
}
protocol DetailsDelegate{
    func onDeleteCart()
}
protocol RetryConnection{
    func reConnected()
}

protocol UserFormDelegate{
    func saveUser(data:UserPedidoModel, payment:String)
}

protocol CheckoutDelegate{
    func close()
}
