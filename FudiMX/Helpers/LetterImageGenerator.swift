//
//  LetterImageGenerator.swift
//  FudiMX
//
//  Created by Cristian Canedo on 26/04/22.
//

import Foundation
import UIKit

class LetterImageGenerator: NSObject {
    
    class func imageWith(on context:UIViewController, name: String?) -> UIImage? {
        guard let view = context.view else{return nil}
        let frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        
        let container = UIView(frame: frame)
        container.backgroundColor = .red
        
        let nameLabel = UILabel(frame: CGRect(x: 20, y: 10, width: container.frame.width - 40, height: 19))
        nameLabel.textAlignment = .center
        nameLabel.textColor = .white
        nameLabel.font = UIFont.boldSystemFont(ofSize: 17.0)
        nameLabel.text = name
        
        container.addSubview(nameLabel)
        
        UIGraphicsBeginImageContext(frame.size)
        if let currentContext = UIGraphicsGetCurrentContext() {
            container.layer.render(in: currentContext)
          let nameImage = UIGraphicsGetImageFromCurrentImageContext()
          return nameImage
        }
        return nil
  }
    
}
