//
//  UtilClass.swift
//  FudiMX
//
//  Created by Cristian Canedo on 11/04/22.
//

import Foundation
import UIKit

class UtilClass{
    static func RealtionCatToEmoji(desc:Int) -> String{
        switch(desc){
        case 0: return "🔥 📢"
        case 1:return "🍣 🍙"
        case 2: return "🍔 🌭"
        case 3:return "🥡 🍜"
        case 4: return "🌮 🥑"
        case 5:return "🦐 🐟"
        case 6: return "🍕 🍝"
        case 7:return "🇲🇽 🌶"
        case 8: return "🥩 🍖"
        case 9:return "🥬 🥗"
        case 10: return "🎂 🍩"
        case 11: return "🍹 🍸"
        default:return "🕐🔜"
        }
    }
    static func RealtionCatToBackGround(desc:Int) -> String{
        switch(desc){
        case 0: return "cat_promo"
        case 1:return "cat_sushi"
        case 2: return "cat_hamburguesas"
        case 3:return "cat_china"
        case 4: return "cat_tacos"
        case 5:return "cat_mariscos"
        case 6: return "cat_pizza"
        case 7:return "cat_mexicana"
        case 8: return "cat_cortes"
        case 9:return "cat_ensaladas"
        case 10: return "cat_pasteles"
        case 11: return "drinks"
        default:return ""
        }
    }
    static func getCurrentDate()-> Date{
        return  Date()
    }
    static func formatDate(format:String)-> String{
        let date = Date()
        let df = DateFormatter()
        df.dateFormat = format
        let dateString = df.string(from: date)
        return  dateString
    }
    
    static func stringArrayToData(stringArray: [String]) -> Data? {
      return try? JSONSerialization.data(withJSONObject: stringArray, options: [])
    }
    static func dataToStringArray(data: Data) -> [String]? {
        return (try? JSONSerialization.jsonObject(with: data, options: [])) as? [String]
    }
    
    static func getDeviceId() -> String{
        return UIDevice.current.identifierForVendor?.uuidString ?? UUID().uuidString
    }
    
    static func addToolbar(on ctx:UIViewController, action:Selector, titleButton:String) -> UIView {
            let barButton = UIBarButtonItem(title: titleButton, style: .plain, target: ctx, action: action)
            let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 44.0))
            let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            toolBar.setItems([flexible, barButton], animated: true)
            return toolBar
    }
}
