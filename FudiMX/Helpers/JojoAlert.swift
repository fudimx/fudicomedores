//
//  JojoAlert.swift
//  FudiMX
//
//  Created by Cristian Canedo on 24/05/22.
//

import Foundation
import UIKit
import Lottie

class JojoAlert{
    
    private let backgroundView:UIView = {
        let background = UIView()
        background.backgroundColor = UIColor.darkGray.withAlphaComponent(0.3)
        background.alpha = 0.0
        return background
    }()
    
    private var alertView:UIView = {
        let alert = UIView()
        alert.backgroundColor = .systemBackground
        //alert.layer.masksToBounds = true
        return alert
    }()
    func loadding(on ctx:UIViewController){
        guard let targetView = ctx.view else {
            return
        }
        let animationName = "loader"
        backgroundView.alpha = 1.0
        backgroundView.isHidden = true
        //UIColor.darkGray.withAlphaComponent(0.3)
        
        backgroundView.frame.size = targetView.frame.size
        backgroundView.frame.origin = CGPoint(x: 0, y: 0)
        
        var animationView: AnimationView?
        animationView = .init(name: animationName)
        animationView!.frame = CGRect(x: 0, y: 0, width: 70, height: 70)
        animationView?.center = backgroundView.center
        animationView!.contentMode = .scaleToFill
        animationView!.loopMode = .loop
        animationView!.animationSpeed = 0.5
        animationView!.play()
        
        backgroundView.addSubview(animationView ?? UIView())
        targetView.addSubview(backgroundView)
        
        
        
        UIView.animate(withDuration: 0.25,animations: {
            self.backgroundView.isHidden = false
        }, completion: {_ in
            self.backgroundView.isHidden = false
        })
    }
    
    func showBottomSheet(on ctx:UIViewController, whit message:String, typeAlert:TypeAlert, buttonTitle:String = "Continuar", selector:Selector? = nil){
        guard let targetView = ctx.view else {
            return
        }
        
        let widthBackgroundView = targetView.frame.width
        let heightBackgroundView = targetView.frame.height
        
        let heightAlert = 350.0
        
        let originYimage = 40.0
        let heighImage = 150.0
        let labelHeigth = message.stringHeight + 30
        
        alertView.alpha = 1
        
        
        backgroundView.frame.size = targetView.frame.size
        backgroundView.frame.origin = CGPoint(x: 0, y: 0)
        
        alertView.frame.size = CGSize(width: CGFloat(widthBackgroundView - 16), height: CGFloat(heightAlert))
        alertView.layer.cornerRadius = 45.0
        self.alertView.frame.origin = CGPoint(x: 8, y: heightBackgroundView)
        
        let close = UIButton(frame: CGRect(x: widthBackgroundView - 60, y: 20, width: 24, height: 24))
        let largeConfig = UIImage.SymbolConfiguration(pointSize: 10, weight: .bold, scale: .large)
        let largeBoldDoc = UIImage(systemName: "xmark", withConfiguration: largeConfig)
        let centerAlert = alertView.frame.size.width/2
        let widthAlert = alertView.frame.width

        
        close.setImage(largeBoldDoc, for: .normal)
        close.layer.cornerRadius = 12
        close.tintColor = .systemGray2
        close.backgroundColor = .systemGray5
        close.addTarget(self, action: #selector(self.dissmisBottomSheet), for: .touchUpInside)
        
        let image = UIImageView(
            frame: CGRect(
                x: centerAlert - (heighImage/2),
                y: originYimage,
                width: heighImage,
                height: heighImage
            ))
        
        image.backgroundColor = .tertiarySystemBackground
        image.image = typeAlert.imageAlert
        image.contentMode = .center
        image.tintColor = typeAlert.tintColor
        image.clipsToBounds = true
        image.contentMode = .scaleAspectFit
        
        let messageLabel = UILabel(frame: CGRect(x: 8, y: image.frame.height + image.frame.origin.y + 5 , width: CGFloat(alertView.frame.width - 32), height: labelHeigth + 20))
        messageLabel.text = message
        messageLabel.textColor = .systemGray
        messageLabel.font = UIFont.systemFont(ofSize: 20.0, weight: .semibold)
        messageLabel.textAlignment = .center
        messageLabel.numberOfLines = 0
        
        let buttonAlert = UIButton(frame: CGRect(x: 16.0, y: heightAlert - 65.0, width: widthAlert - 32.0, height: 45.0))
        buttonAlert.cornerRadius = 10.0
        buttonAlert.backgroundColor = UIColor.systemGray5
        buttonAlert.setTitleColor( UIColor.label , for: UIControl.State.normal)
        buttonAlert.setTitle(buttonTitle , for: UIControl.State.normal)
        buttonAlert.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16.0)
        let selectorAlert = selector != nil ? selector! : #selector(self.dissmisBottomSheet)
        let contextalert:Any = selector != nil ? ctx : self
        buttonAlert.addTarget(contextalert, action: selectorAlert, for: UIControl.Event.touchUpInside)
        
        alertView.addSubview(close)
        alertView.addSubview(image)
        alertView.addSubview(messageLabel)
        alertView.addSubview(buttonAlert)
        backgroundView.addSubview(alertView)
        targetView.addSubview(backgroundView)
        VibrationAlert(type: typeAlert)
        
       
        
        UIView.animate(withDuration: 0.25,animations: {
            self.backgroundView.alpha = 1.0
            self.alertView.frame.origin = CGPoint(x: 8, y: heightBackgroundView - CGFloat(heightAlert + 10) )
            //self.backgroundView.frame.origin = CGPoint(x: 0, y: 0)
        }, completion: {_ in
            self.backgroundView.alpha = 1.0
            //self.backgroundView.frame.origin = CGPoint(x: 0, y: 0)
            self.alertView.frame.origin = CGPoint(x: 8, y: heightBackgroundView - CGFloat(heightAlert + 10) )
            
        })
        
    }
    
    @objc private func dissmisBottomSheet(){
        dismissAlert()
    }
    
    func dismissAlert(){
        alertView.alpha = 0
        for view in alertView.subviews{
            view.removeFromSuperview()
        }
        UIView.animate(withDuration: 0.25,animations:  {
            self.backgroundView.alpha = 0
        }, completion: { done in
            if(done){
                self.alertView.removeFromSuperview()
                self.backgroundView.removeFromSuperview()
            }
        })
    }
    
    func stopLoadding(){
        for view in backgroundView.subviews{
            view.removeFromSuperview()
        }
        UIView.animate(withDuration: 0.25,animations:  {
            self.backgroundView.alpha = 0
        }, completion: { done in
            if(done){
                self.backgroundView.removeFromSuperview()
            }
        })
    }
    
    
}
extension TypeAlert{
    var tintColor: UIColor {
        switch self {
        case .Success: return UIColor(named: "Success")!
        case .Error: return UIColor(named: "Error")!
        case .Warning: return UIColor(named: "Warning")!
        case .Info: return UIColor(named: "Info")!
        }
    }
    
    var imageAlert: UIImage {
        switch self {
        case .Success: return UIImage(systemName: "checkmark.circle.fill")!
        case .Error: return UIImage(systemName: "xmark.circle.fill")!
        case .Warning: return UIImage(systemName: "exclamationmark.triangle.fill")!
        case .Info: return UIImage(systemName: "info.circle.fill")!
        }
    }
}

