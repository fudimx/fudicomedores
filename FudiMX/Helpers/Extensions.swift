//
//  Extensions.swift
//  FudiMX
//
//  Created by Cristian Canedo on 07/04/22.
//

import Foundation
import UIKit

extension String {
    var stringWidth: CGFloat {
        let constraintRect = CGSize(width: UIScreen.main.bounds.width, height: .greatestFiniteMagnitude)
        let boundingBox = self.trimmingCharacters(in: .whitespacesAndNewlines).boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)], context: nil)
        return boundingBox.width
    }

    var stringHeight: CGFloat {
        let constraintRect = CGSize(width: UIScreen.main.bounds.width, height: .greatestFiniteMagnitude)
        let boundingBox = self.trimmingCharacters(in: .whitespacesAndNewlines).boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)], context: nil)
        return boundingBox.height
    }
}
extension ErrorResponse{
    func messageError(msg:String) -> String{
        switch(self.codeError){
        case 400: return "Elemento no accesible, intente de nuevo más tarde."
        case 401: return "No estas autorizado para relizar esta operación."
        case 404: return "Elemento no encontrado, intente de nuevo más tarde."
        case 409: return "Elemento no accesible, intente de nuevo más taarde."
        case 500: return "No se ha podido conectar con el servidor porfavor intente más tarde."
        case 0: return "Error de conexión, asegurate de estar conectado a internet."
        default: return msg != "" ? msg : "Error al contactar nuestros servicios, intentelo mas tarde"
        }
    }
}
