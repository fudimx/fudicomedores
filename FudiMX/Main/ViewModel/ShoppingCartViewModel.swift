//
//  ShoppingCartViewController.swift
//  FudiMX
//
//  Created by Cristian Canedo on 20/04/22.
//

import Foundation

class ShoppingCartViewModel{
    var ref:Referencerequest!
    var refreshData = {(code:Referencerequest) -> () in}
    var idOrder:String?
    var shoppingCartResponse:[ShoppingCartModel] = []{didSet{refreshData(ref)}}
    var deleteCartResponse:Bool = false{didSet{refreshData(ref)}}
    var deleteByIdResponse:Bool = false{didSet{refreshData(ref)}}
    var createOrderResponse:Bool = false{didSet{refreshData(ref)}}
    var userForm:UserPedidoModel?
    var payment = 0.0
    var totalForPay = 0.0
    var onBuy = false
    
    func getShoppingCart(_ reference:Referencerequest){
        self.ref = reference
        let cartItems = ShoppingCartLocalManager.shared.getModels() ?? []
        cartItems.forEach{it in
            self.totalForPay += it.total ?? 0.0
        }
        self.shoppingCartResponse = cartItems
    }
    func deleteCart(_ reference:Referencerequest){
        self.ref = reference
        let response = ShoppingCartLocalManager.shared.deleteModel()
        self.deleteCartResponse = response
    }
    func deleteById(_ reference:Referencerequest, idModel:String){
        self.ref = reference
        let response = ShoppingCartLocalManager.shared.deleteModelById(id: idModel)
        self.deleteByIdResponse = response
    }
    func createOrder(_ reference:Referencerequest, data:OrderModel){
        self.ref = reference
        OrdersLocalManager.shared.saveModels(model: data) { response, idOrder  in
            self.onBuy = response
            self.idOrder = idOrder
            self.createOrderResponse = response
        }
    }

    func generateOrderItems() -> [OrderItemModel]{
        var itemsOrders:[OrderItemModel] = []
        self.shoppingCartResponse.forEach{ it in
            let orderItemModel = OrderItemModel.init(idProduct: it.idProduct ?? "", idShoppingCart: it.id ?? "", notes: it.notes ?? "", quantity: it.quantity ?? 0, total: it.total ?? 0, addOns: it.addOns, sizes: it.sizes)
            itemsOrders.append(orderItemModel)
        }
        return itemsOrders
    }

}
