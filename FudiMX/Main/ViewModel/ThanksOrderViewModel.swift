//
//  ThanksOrderViewModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 08/05/22.
//

import Foundation

class ThanksOrderViewModel{
    var reference:Referencerequest!
    var refreshData = {(code:Referencerequest) -> () in}
    var getFolioResponse:String? = nil{didSet{refreshData(reference)}}
    var createOrderResponse:CreateOrderResponse? = nil{didSet{refreshData(reference)}}
    var errorResponse:ErrorResponse? = nil{didSet{refreshData(reference)}}
    var checkoutModel:CreateOrderModel?
    
    func initModel(data:CreateOrderModel){checkoutModel = data}
    func setPedido(data:PedidoModel){checkoutModel?.pedido = data}
    func setFolio(data:String){checkoutModel?.folioPedido = data}
    func setIsInvited(data:Bool){ checkoutModel?.pedido?.bCompraInvitado = data}
    func setIsDelivery(data:Bool){ checkoutModel?.pedido?.bRecoger = data}
    func setDeliveryPrice(data:Double){ checkoutModel?.pedido?.costoEnvio = data}
    func setDate(data:String){ checkoutModel?.pedido?.fechaPedido = data}
    func setMethodPay(data:MethodPayModel){ checkoutModel?.pedido?.formaDePago = data}
    func setIndex(data:Int){ checkoutModel?.pedido?.index = data}
    func setDrinks(data:[BebidasModel]){ checkoutModel?.pedido?.lstBebidas = data}
    func setAddOns(data:[ComplementosModel]){ checkoutModel?.pedido?.lstComplementos = data}
    func setOrders(data:[PlatillosModel]){ checkoutModel?.pedido?.lstPlatillos = data}
    func setPromos(data:[PromocionesModel]){ checkoutModel?.pedido?.lstPromociones = data}
    func setIdCategories(data:Int){ checkoutModel?.pedido?.nIdCategoria = data}
    func setIdCoupon(data:String){ checkoutModel?.pedido?.nIdCupon = data}
    func setIdRest(data:String){ checkoutModel?.pedido?.nIdRestaurante = data}
    func setIdSucursal(data:String){ checkoutModel?.pedido?.nIdSucursal = data}
    func setRestName(data:String){ checkoutModel?.pedido?.nombreRestaurante = data}
    func setNameExt(data:String){ checkoutModel?.pedido?.nombreSucursal = data}
    func setPlayerId(data:String){ checkoutModel?.pedido?.playerId = data}
    func setRegDelivery(data:String){ checkoutModel?.pedido?.regionEtrega = data}
    func setstatusDelivery(data:Int){ checkoutModel?.pedido?.statusPedido = data}
    func setSubTotal(data:Double){ checkoutModel?.pedido?.subTotal = data}
    func setBuyType(data:Int){ checkoutModel?.pedido?.tipoCompra = data}
    func setTotal(data:Double){ checkoutModel?.pedido?.total = data}
    func setUser(data:UserPedidoModel){ checkoutModel?.pedido?.usuario = data}
    
    func getOrder(idOrder:String)-> [OrderItemModel]{
        guard let order = OrdersLocalManager.shared.getModelByID(id: idOrder) else{
            return []
        }
        let orders = OrderItemLocalManager.shared.getModels(idOrder: order.id ?? "") ?? []
       return orders
    }
    
    func getFolio(_ ref:Referencerequest, id:String){
        self.reference = ref
        getFolioOrderRequest(data: id) { folio, error in
            guard let folioRes = folio else{
                self.errorResponse = error
                return
            }
            self.getFolioResponse = folioRes
        }
    }
    func createOrder(_ ref:Referencerequest){
        self.reference = ref
        guard let data = checkoutModel else{
            self.errorResponse = ErrorResponse(message: "Modelo incompleto", codeError: 0)
            return
        }
        createOrderRequest(data: data) { result, error in
            if result != nil {
                self.createOrderResponse = result
            }else{
                self.errorResponse = error
            }
        }
    }
}

