//
//  NoInternetViewModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 25/05/22.
//

import Foundation
class NoInternetViewModel{
    var reference:Referencerequest!
    var refreshData = {(status:Referencerequest) -> () in}
    
    var response:Bool = false{didSet{refreshData(self.reference)}}
    
    func checkConection(_ ref:Referencerequest){
        self.reference = ref
        
    }
}
