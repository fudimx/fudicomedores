//
//  LocationsViewmodel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 14/04/22.
//

import Foundation

class LocationsViewModel{
    var refReq:Referencerequest!
    var refreshData = {(code:Referencerequest) -> () in}
    var getLocationsResponse:[LocationModel] = []{didSet{refreshData(refReq)}}
    
    
    func getlocations(_ ref:Referencerequest){
        self.refReq = ref
        self.getLocationsResponse = LocationLocalManager.shared.getModels() ?? []
    }
}
