//
//  WelcomeViewModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 14/04/22.
//

import Foundation

class WelcomeViewModel{
    var refReq:Referencerequest!
    var refreshData = {(code:Referencerequest) -> () in}
    var getBranchesRequestResponse:[BranchModel] = []{didSet{refreshData(refReq)}}
    var saveBranchsResponse:Bool? = nil{didSet{refreshData(refReq)}}
    var errorResponse:ErrorResponse? = nil{didSet{refreshData(.errorResponse)}}
    
    func getBranches(_ ref:Referencerequest, idLocation:String){
        self.refReq = ref
        getBranchesRequest(idLocation: idLocation) { result in
            self.getBranchesRequestResponse = result
        } failure: { error in
            self.errorResponse = error
        }
    }
    func saveLocationSelected(model:LocationModel){
        UserPreferencesModel.shared.initLocationModel(data: model)
    }
    func saveBranch(_ ref:Referencerequest, data toSave:[BranchModel]){
        self.refReq = ref
        BranchLocalManage.shared.saveModels(models:toSave){ res in
            self.saveBranchsResponse = res
        }
    }
}
