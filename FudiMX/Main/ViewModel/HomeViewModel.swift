//
//  HomeViewmodel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 11/04/22.
//

import Foundation

class HomeViewModel{
    var ref:Referencerequest!
    var refreshData = {(code:Referencerequest) -> () in}
    
    var isEmpty = false
    var haveDrinks = -1
    var getBranches:[BranchModel] = []{didSet{refreshData(ref)}}    
    func getBrnaches(_ ref:Referencerequest, idLocation:String){
        self.ref = ref
        self.getBranches = BranchLocalManage.shared.getModelsById(idLocation: idLocation) ?? []
        
        self.getBranches.enumerated().forEach{ index, it in
            if let isDrinkExist = it.barraBebidas, isDrinkExist{
                self.haveDrinks = index
                return
            }
        }
        if getBranches.isEmpty {
            isEmpty = true
        }
    }
}
