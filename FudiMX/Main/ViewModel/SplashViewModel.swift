//
//  SplashViewModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 10/04/22.
//

import Foundation

class SplashViewModel{
    var refReq:Referencerequest!
    var refreshData = {(code:Referencerequest) -> () in}    
    var saveLocationsResponse:Bool? = nil{didSet{refreshData(refReq)}}
    
    var getLocationsResponse:[LocationModel] = []{didSet{refreshData(refReq)}}
    var errorResponse:ErrorResponse? = nil{didSet{refreshData(.errorResponse)}}
    
    
    func getLocationsRequest(_ ref:Referencerequest){
        self.refReq = ref
        getLocationRequest { result in
            self.getLocationsResponse = result
        } failure: { error in
            self.errorResponse = error
        }
    }
    func saveLocations(_ ref:Referencerequest, data toSave:[LocationModel]){
        self.refReq = ref
        LocationLocalManager.shared.saveModels(models: toSave) { response in
            self.saveLocationsResponse = response
        }
    }
    func savePoligons(){
        getLocationsResponse.forEach{it in
            PoligonRegionLocalModel.shared.saveModels(models: it.poligonoRegion, idLocation: it._id)
        }
    }
}
