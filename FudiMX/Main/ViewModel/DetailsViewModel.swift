//
//  DetailsViewmodel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 18/04/22.
//

import Foundation

class DetailsViewModel{
    var refReq:Referencerequest!
    var refreshData = {(code:Referencerequest) -> () in}
    var getMenuResponse:[MenuModel] = []{didSet{refreshData(refReq)}}
    var getMenuReqResponse:[MenuModel] = []{didSet{refreshData(refReq)}}
    var getProductsResponse:[ProductsModel] = []{didSet{refreshData(refReq)}}
    var getAllProductsResponse:[ProductsModel] = []{didSet{refreshData(refReq)}}
    var deleteCartResponse:Bool? = nil{didSet{refreshData(refReq)}}
    var saveMenuResponse:Bool = false{didSet{refreshData(refReq)}}
    var errorResponse:ErrorResponse? = nil{didSet{refreshData(refReq)}}
    var idBranch = ""

    func getInitialMenu(_ ref:Referencerequest, idBranch:String){
        self.refReq = ref
        getCargaInicial(idBranch: idBranch) { result in
            self.getMenuReqResponse = result
        } failure: { error in
            self.errorResponse = error
        }
    }
    func saveMenu(_ ref:Referencerequest){
        self.refReq = ref
        MenuLocalManager.shared.saveModels(models: self.getMenuReqResponse) { res in
            self.saveMenuResponse = res
            if res{
                let currentDate = Date()
                UserPreferencesModel.shared.initLastMenuDate(date: currentDate)
            }
        }
    }
    
    func getMenu(_ ref:Referencerequest, idBranch:String){
        self.refReq = ref
        let response = MenuLocalManager.shared.getModels(idBranch: idBranch) ?? []
        let currentDate = Date()
        let lastDate = UserPreferencesModel.shared.getLastMenuDate() ?? Date()
        let compare = Calendar.current.isDate(currentDate, equalTo: lastDate, toGranularity: .day)
        if response.isEmpty || !compare{
            getInitialMenu(.getMenuRequest, idBranch: idBranch)
        }else{
            getMenuResponse = response
            getProductsResponse = response[0].lstPlatillos
        }
    }

    func getProductsByMenu(_ ref:Referencerequest, index:Int){
        self.refReq = ref
        self.getProductsResponse = getMenuResponse[index].lstPlatillos
    }
    func getProductsList(idBranch:String) -> [ProductsModel]{
        return ProductsLocalmanager.shared.getModelsByBranch(idBranch: idBranch) ?? []
    }
    func getShoppingCartActive() -> WatchCartItems?{
        return ShoppingCartLocalManager.shared.getCart()
    }
    func deleteCart(_ reference:Referencerequest){
        self.refReq = reference
        deleteCartResponse = ShoppingCartLocalManager.shared.deleteModel()
    }
}
