//
//  HeaderHomeViewmodel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 06/04/22.
//

import Foundation

class HeaderHomeViewmodel{
    var ref:Referencerequest!
    var refresData = {(ref:Referencerequest) -> () in}
    
    var getBannerResponse:[BannerModel] = []{didSet{refresData(ref)}}
    var isEmpty = false
    
    func getbanner(_ ref:Referencerequest){
        self.ref = ref
        
        _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(fireTimer), userInfo: nil, repeats: false)
    }
    @objc func fireTimer() {
        let response = BannerLocalManager.shared.getModels() ?? []
        isEmpty = response.isEmpty
        self.getBannerResponse = response
        
    }
}
