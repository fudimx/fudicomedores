//
//  ProductDetailViewModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 09/04/22.
//

import Foundation

class ProductDetailViewModel{
    
    var numProducts = 1
    var ref:Referencerequest!
    var refreshData = {(code:Referencerequest) -> () in}
    var notes = ""
    var addOndsResponse:[AddOnsModel] = []{didSet{refreshData(ref)}}
    
    var addOns:[String] = []{didSet{
        refreshData(.setAddOns)
    }}
    var deleteCartResponse:Bool = false{didSet{refreshData(ref)}}
    var sizeSelected:SizeModel?
    
    var saveItemResponse:Bool? = nil{didSet{refreshData(ref)}}
    
    func addCart(_ reference:Referencerequest, data:ShoppingCartModel ){
        self.ref = reference
        ShoppingCartLocalManager.shared.saveModels(model: data) { res in
            self.saveItemResponse = res
        }
    }
    func setSizesCart(_ reference:Referencerequest, data:[SizeCartModel], idShoppingCart:String){
        self.ref = reference
        SizeCartLocalManage.shared.saveModels(models: data, idShoppingCart: idShoppingCart)
    }
    func deleteCart(_ reference:Referencerequest){
        self.ref = reference
        let response = ShoppingCartLocalManager.shared.deleteModel()
        self.deleteCartResponse = response
    }
    func getAddOns(_ ref:Referencerequest, idProduct:String){
        self.ref = ref
        addOndsResponse = AddOnsLocalManage.shared.getModels(idProduct: idProduct) ?? []
    }
}
