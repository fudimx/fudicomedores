//
//  ScannCodeViewModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 23/05/22.
//

import Foundation

class ScannCodeViewModel{
    var refReq:Referencerequest!
    var refreshData = {(code:Referencerequest) -> () in}
    var getLocationsResponse:[LocationModel] = []{didSet{refreshData(refReq)}}
    
    
    func getlocations(_ ref:Referencerequest){
        self.refReq = ref
        self.getLocationsResponse = LocationLocalManager.shared.getModels() ?? []
    }
}
