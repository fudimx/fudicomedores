//
//  SearchViewController.swift
//  FudiMX
//
//  Created by Cristian Canedo on 08/04/22.
//

import UIKit

class SearchViewController: UIViewController {

    @IBOutlet weak var tvList:UITableView!
    @IBOutlet weak var searchBar:UISearchBar!
    
    var delegate:SearchControllerDelegate?
    var firstTableArray:[ProductsModel] = []
    var filteredData: [ProductsModel]!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    private func setupTableView(){
        filteredData = firstTableArray
        let nib = UINib(nibName: "TableViewCell", bundle: nil)
        tvList.register(nib, forCellReuseIdentifier: TableViewCell.identifier)
    }
    @IBAction func closeAction(_ sender:UIButton){
        self.dismiss(animated: true)
    }
}
