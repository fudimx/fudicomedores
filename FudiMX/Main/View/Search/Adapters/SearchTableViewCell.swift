//
//  SearchTableViewCell.swift
//  FudiMX
//
//  Created by Cristian Canedo on 09/04/22.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

    static let identifier = "searchTableViewCell"
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
