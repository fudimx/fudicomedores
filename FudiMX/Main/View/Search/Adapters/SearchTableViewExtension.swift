//
//  SearchTableViewExtension.swift
//  FudiMX
//
//  Created by Cristian Canedo on 09/04/22.
//

import Foundation
import UIKit

extension SearchViewController: UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.identifier, for: indexPath) as! TableViewCell
        let item = self.filteredData[indexPath.row]
        let price = item.lstTamanios.isEmpty ? 0.0 : item.lstTamanios[0].nPrecio
        
        cell.lblTitle.text = item.cNombrePlatillo
        cell.lblDescription.text = item.cDescripcion
        cell.lblPrice.text = "$\(price)"
        return cell
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredData = searchText.isEmpty ? firstTableArray : firstTableArray.filter({(dataString: ProductsModel) -> Bool in
            return dataString.cNombrePlatillo.range(of: searchText, options: .caseInsensitive) != nil
        })
        tvList.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let callback = self.delegate else{return}
        self.dismiss(animated: true) {
            callback.onItemSelected(item: self.filteredData[indexPath.row])
        }
    }
}

