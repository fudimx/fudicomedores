//
//  CheckoutViewController.swift
//  FudiMX
//
//  Created by Cristian Canedo on 21/04/22.
//

import UIKit

class CheckoutViewController: UIViewController {

    @IBOutlet weak var tvList:UITableView!
    let mViewModel = CheckoutViewModel()
    var idOrder:String!
    let alert = JojoAlert()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Detalle del pedido"
        self.binderViewModel()
        self.setupTableView()
        self.mViewModel.getOrders(.getOrder, idOrder: idOrder)
    }
    
    func binderViewModel(){
        self.mViewModel.refreshData = {[weak self] code in
            switch(code){
            case .getOrder: self?.onGetOrdersEnded()
            default: self?.onErrorResponseEnded()
            }
        }
    }
    
    func onGetOrdersEnded(){
        tvList.reloadData()
    }
    func setupTableView(){
        let nib = UINib(nibName: "TableViewCell", bundle: nil)
        tvList.register(nib, forCellReuseIdentifier: TableViewCell.identifier)
    }
    func onErrorResponseEnded(){
        alert.showBottomSheet(on:self , whit: "Ha Ocurrido un error" , typeAlert: .Warning, selector: nil)
    }
}

