//
//  CheckoutExtension.swift
//  FudiMX
//
//  Created by Cristian Canedo on 22/05/22.
//

import Foundation
import UIKit

extension CheckoutViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let response = self.mViewModel.orderResposne else {return 0}
        return response.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.identifier, for: indexPath) as! TableViewCell
        guard let item = self.mViewModel.orderResposne?.items[indexPath.row], let protuctItem = ProductsLocalmanager.shared.getItemById(id: item.idProduct)  else{return cell}
        cell.lblPrice.text = "$\(item.total ?? 0.0)"
        cell.lblTitle.text = protuctItem.cNombrePlatillo
        cell.lblDescription.text = UtilClass.RealtionCatToEmoji(desc: Int(protuctItem.nIdCategoria) ?? -1)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
}
