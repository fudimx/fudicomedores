//
//  ShoppingCartViewController.swift
//  FudiMX
//
//  Created by Cristian Canedo on 10/04/22.
//

import UIKit

class ShoppingCartViewController: UIViewController {

    @IBOutlet weak var tvList:UITableView!
    @IBOutlet weak var btnOrder:UIButton!
    
    @IBOutlet weak var lblNameSucursal:UILabel!
    @IBOutlet weak var lblNameMethodPay:UILabel!
    
    @IBOutlet weak var lblDescriptionRestaurant:UILabel!
    @IBOutlet weak var lblDescriptionMethodPay:UILabel!
    @IBOutlet weak var lblDescriptionTotal:UILabel!
    
    @IBOutlet weak var icAddress:UIImageView!
    @IBOutlet weak var icMethodPay:UIImageView!
    @IBOutlet weak var icTotal:UIImageView!
    
    
    var delegate:DetailsDelegate!
    let mViewModel = ShoppingCartViewModel()
    var delegateChackout:CheckoutDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        initUI()
        binderVIewModel()
        mViewModel.getShoppingCart(.getShoppingCart)
    }
    func binderVIewModel(){
        mViewModel.refreshData = {[weak self] (code) in
            switch(code){
            case .getShoppingCart:
                self?.onGetShoppingCart()
            case .deleteShoppingCart:
                self?.onDeleteCart()
            case .deleteItemCart:
                self?.onDeleteItemCart()
            case .saveOrder:
                self?.onSaveOrder()
            default:
                break
            }
        }
    }
    func initUI(){
        let rTrash = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deleteAction))
        rTrash.tintColor = .label
        self.navigationItem.rightBarButtonItems = [rTrash]
        
        let largeConfig = UIImage.SymbolConfiguration(pointSize: 17, weight: .semibold, scale: .large)
        let largeBoldDoc = UIImage(systemName: "xmark", withConfiguration: largeConfig)
        
        let lClose = UIBarButtonItem(image: largeBoldDoc, style: .plain, target: self, action: #selector(closeAction))
        lClose.tintColor = .label
        
        self.navigationItem.leftBarButtonItem = lClose
        
        if let methodPay = UserPreferencesModel.shared.getMethodPay(){
            lblNameMethodPay.text = methodPay
        }else{
            lblNameMethodPay.text = "Efectivo"
            lblDescriptionMethodPay.text = "Pagar en mostrador"
        }
        if let locationModel = UserPreferencesModel.shared.getLocationModel(){
            lblNameSucursal.text =  "Suc." + locationModel.nombreRegion
        }else{
            lblNameSucursal.text = "Sin sucursal"
        }
        if let restaurantModel = UserPreferencesModel.shared.getBranchModel(){
            lblDescriptionRestaurant.text =  restaurantModel.cNombreSucursal
        }else{
            lblDescriptionRestaurant.text = "Sin restaurante"
        }
        
    }
    
    func setupTableView(){
        let nib = UINib(nibName: "TableViewCell", bundle: nil)
        tvList.register(nib, forCellReuseIdentifier: TableViewCell.identifier)
    }
    @objc func deleteAction(){
        self.mViewModel.deleteCart(.deleteShoppingCart)
    }

    @objc func closeAction(){
        self.chekCart()
        self.dismiss(animated: true)
    }
    @IBAction func payAction(_ sender:UIButton){
        let vc = UserFormViewController()
        vc.formUserDelegate = self
        vc.totalToPay = mViewModel.totalForPay
        self.present(vc, animated: true)
    }
    func chekCart(){
        let respId = mViewModel.deleteByIdResponse
        let resp = mViewModel.deleteCartResponse
        if respId || resp{
            delegate.onDeleteCart()
        }
    }
    func onGetShoppingCart(){
        let cartList = mViewModel.shoppingCartResponse
        let isempty = !cartList.isEmpty
        btnOrder.isEnabled = isempty
        btnOrder.backgroundColor = isempty ? UIColor(red: 253/255, green: 85/255, blue: 38/255, alpha: 1.0 ): .lightGray
        lblDescriptionTotal.text = "$\(self.mViewModel.totalForPay)"
        tvList.reloadData()
    }
    func onDeleteCart(){
        self.mViewModel.getShoppingCart(.getShoppingCart)
        if mViewModel.onBuy{
            guard let id = mViewModel.idOrder else{return}
            let vc = ThanksOrderViewController()
            vc.idOrder = id
            vc.userForm = self.mViewModel.userForm
            vc.payment = self.mViewModel.payment
            vc.checkoutDelegate = self.delegateChackout
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func onDeleteItemCart(){
        self.mViewModel.getShoppingCart(.getShoppingCart)
    }
    func onSaveOrder(){
        let response = self.mViewModel.createOrderResponse
        if response {
            mViewModel.deleteCart(.deleteShoppingCart)
        }
    }
    func getTotalShoppingCart()->Double{
        return 0.0
    }
}
