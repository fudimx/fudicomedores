//
//  ShoppingCartExtension.swift
//  FudiMX
//
//  Created by Cristian Canedo on 10/04/22.
//

import Foundation
import UIKit

extension ShoppingCartViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mViewModel.shoppingCartResponse.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.identifier, for: indexPath) as! TableViewCell
        let item = self.mViewModel.shoppingCartResponse[indexPath.row]
        cell.lblPrice.text = "$\(item.total ?? 0.0)"
        cell.lblTitle.text = item.nameProduct
        cell.lblDescription.text = "x\(item.quantity ?? 0)"
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle:   UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {

            let item = self.mViewModel.shoppingCartResponse[indexPath.row]
            self.mViewModel.shoppingCartResponse.remove(at: indexPath.row)
            self.mViewModel.deleteById(.deleteItemCart, idModel: item.id!)
            Vibration.heavy.vibrate()
            tableView.reloadData()

        }
    }
}
extension ShoppingCartViewController: UserFormDelegate{
    func saveUser(data: UserPedidoModel, payment: String) {
        var mOrderModel:OrderModel = OrderModel()
        mOrderModel.idBranch = UserPreferencesModel.shared.getBranchModel()?._id ?? "idBranch"
        mOrderModel.idMethodPay = UserPreferencesModel.shared.getMethodPay() ?? "idMethodPay"
        mOrderModel.idLocation = UserPreferencesModel.shared.getLocationModel()?._id ?? "idLocation"
        mOrderModel.date = UtilClass.getCurrentDate()
        mOrderModel.status = ""
        mOrderModel.total = self.mViewModel.totalForPay
        mOrderModel.items = self.mViewModel.generateOrderItems()
        self.mViewModel.userForm = data
        self.mViewModel.payment = Double(payment) ?? 0.0
        self.mViewModel.createOrder(.saveOrder, data: mOrderModel)
    }
}
