//
//  ScannCodeViewController.swift
//  FudiMX
//
//  Created by Cristian Canedo on 09/04/22.
//

import UIKit
import AVFoundation

class ScannCodeViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate{

    @IBOutlet weak var cammeraViewer:UIView!

    
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    var delegate:WelcomeControllerDelegate?
    let mViewModel = ScannCodeViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let authStatus = AVCaptureDevice.authorizationStatus(for: .video)
           switch authStatus {
           case .authorized: setupPreview()
           case .notDetermined: alertPromptToAllowCameraAccessViaSetting()
           default: alertToEncourageCameraAccessInitially()
        }
        mViewModel.getlocations(.getLocations)
    }
    override func viewWillAppear(_ animated: Bool) {
        
        if (captureSession?.isRunning == false) {
                captureSession.startRunning()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
            
        }
    }
    
    func alertPromptToAllowCameraAccessViaSetting() {
        AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) -> Void in
               if granted == true {
                   self.setupPreview()
               } else {
                   DispatchQueue.main.async {
                       let alert = UIAlertController(title: "Permisos denegados", message: "No tienes los permisos necesarios para acceder a ala camara.", preferredStyle: .alert)
                       alert.addAction(UIAlertAction(title: "Cerrar", style: .default){_ in
                           self.dismiss(animated: true, completion: nil)
                       })
                       self.present(alert, animated: true, completion: nil)
                   }
               }
           })
    }
    func alertToEncourageCameraAccessInitially() {
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Permisos denegados", message: "No tienes los permisos necesarios para acceder a ala camara.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Dar permisos", style: .cancel, handler: { (alert) -> Void in
                UIApplication.shared.openURL(URL(string: UIApplication.openSettingsURLString)!)
               }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func setupPreview(){
        captureSession = AVCaptureSession()
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }

        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }

        let metadataOutput = AVCaptureMetadataOutput()

        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)

            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }

        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        cammeraViewer.layer.addSublayer(previewLayer)
        captureSession.startRunning()
    }
    
    func failed() {
            let ac = UIAlertController(title: "No se pudo escanear", message: "Su dispositivo no admite escanear un código de un artículo. Utilice un dispositivo con cámara.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
            captureSession = nil
    }
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
            captureSession.stopRunning()

            if let metadataObject = metadataObjects.first {
                guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
                guard let stringValue = readableObject.stringValue else { return }
                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                found(code: stringValue)
            }
    }
    
    func found(code: String) {
        dismiss(animated: true){
            guard let del = self.delegate else{return}
            
            
            if code.range(of: "https://download.fudimx.com/") != nil {
                let clearDomaain = code.replacingOccurrences(of: "https://download.fudimx.com/", with: "")
                let arrayIds = clearDomaain.components(separatedBy: "/")
                let locationId = arrayIds.count > 0 ? arrayIds[0] : ""
               // let tableId = arrayIds.count > 1 ? arrayIds[1] : ""
                
                let response = self.ifExistLocation(idLocation: locationId)
                self.dismiss(animated: true) {
                    del.onLocationSelected(item: response)
                }
            }else{
                self.dismiss(animated: true) {
                    del.onLocationSelected(item: nil)
                }
            }
        }
    }
    
    func ifExistLocation(idLocation:String)-> LocationModel?{
        let locations = mViewModel.getLocationsResponse
        var response:LocationModel?
        locations.forEach{it in
            if idLocation == it._id{
                response = it
            }
        }
        return response
    }
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
            return .portrait
    }
    
    @IBAction func close(_ sender:UIButton){
        self.dismiss(animated: true)
    }
}
