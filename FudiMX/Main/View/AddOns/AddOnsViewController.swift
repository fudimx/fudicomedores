//
//  AddOnsViewController.swift
//  FudiMX
//
//  Created by Cristian Canedo on 26/04/22.
//

import UIKit

class AddOnsViewController: UIViewController {

    @IBOutlet weak var tvList:UITableView!
    @IBOutlet weak var emptyView:UIView!
    
    let mViewModel = AddOnsViewModel()
    var idProduct:String?
    var delegateAddons:ProductDetailsDelegate?
    var arrayAddOns:[AddOnsModel] = []
    let alert = JojoAlert()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mViewModel.addOndsResponse = arrayAddOns
        self.title = "Seleccionar complementos"
        setupTableView()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }

    func setupTableView(){
        let nib = UINib(nibName: "AddonsTableViewCell", bundle: nil)
        tvList.register(nib, forCellReuseIdentifier: AddonsTableViewCell.identifier)
        tvList.reloadData()
    }

    @IBAction func selectAction(_ sender:UIButton){
        guard let delegate = delegateAddons else{return}
        var addons:[String] = []
        self.mViewModel.addOndsResponse.forEach{it in
            it.complementosSelect?.enumerated().forEach{index,jt in
                if jt{
                    let obj = it.complementos[index]
                    addons.append(obj)
                }
            }
        }
        if addons.count > 0{
            self.dismiss(animated: false, completion: {
                
                delegate.onAddOnsSelected(addOns: addons)
            })
        }else{
            alert.showBottomSheet(on: self, whit: "Debes seleccionar almenos un elemento", typeAlert: .Warning)
        }        
    }
    @IBAction func close(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
}
