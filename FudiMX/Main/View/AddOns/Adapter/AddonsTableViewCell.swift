//
//  AddonsTableViewCell.swift
//  FudiMX
//
//  Created by Cristian Canedo on 26/04/22.
//

import UIKit

class AddonsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDescription:UILabel!
    @IBOutlet weak var icCheck:UIImageView!
    
    static let identifier = "addonsTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
