//
//  AddOnsExtension.swift
//  FudiMX
//
//  Created by Cristian Canedo on 26/04/22.
//

import Foundation
import UIKit

extension AddOnsViewController:UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.mViewModel.addOndsResponse.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let item = self.mViewModel.addOndsResponse[section]
        let array = item.complementos
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AddonsTableViewCell.identifier, for: indexPath) as! AddonsTableViewCell
        
        let item = self.mViewModel.addOndsResponse[indexPath.section].complementos[indexPath.row]
        cell.lblDescription.text = item
        
        cell.icCheck.isHidden = !(self.mViewModel.addOndsResponse[indexPath.section].complementosSelect?[indexPath.row] ?? false)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let item = self.mViewModel.addOndsResponse[section]
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 45))
        let title = UILabel(frame: CGRect(x: 10, y: 2, width: self.view.frame.width - 20, height: 25))
        title.text = item.pasoPreparacion
        title.textColor = .label
        title.font = UIFont.systemFont(ofSize: 17.0, weight: .black)
        
        view.addSubview(title)
       return view
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        clearSection(section: indexPath.section)
        self.mViewModel.addOndsResponse[indexPath.section].complementosSelect?[indexPath.row] = true
        tvList.reloadData()
    }
    func clearSection(section: Int){
        self.mViewModel.addOndsResponse[section].complementos.enumerated().forEach{ index, it in
            self.mViewModel.addOndsResponse[section].complementosSelect?[index] = false
        }
    }
}
