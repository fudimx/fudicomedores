//
//  ThanksOrderViewController.swift
//  FudiMX
//
//  Created by Cristian Canedo on 21/04/22.
//

import UIKit
import Lottie

class ThanksOrderViewController: UIViewController {

    @IBOutlet weak var animationView:AnimationView!
    @IBOutlet weak var viewDesc:UIView!
    
    var idOrder:String!
    var userForm:UserPedidoModel!
    var checkoutDelegate:CheckoutDelegate!
    var payment:Double = 0.0
    
    let mViewModel = ThanksOrderViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        binderViewModel()
        guard let restaurantModel = UserPreferencesModel.shared.getBranchModel()
        else{return}
        self.navigationItem.setHidesBackButton(true, animated: true)
        mViewModel.getFolio(.getFolioOrder, id: restaurantModel.nIdRestaurante)
        lottieSetup()
    }

    func lottieSetup(){
        animationView!.frame = view.bounds
        animationView!.contentMode = .scaleAspectFit
        animationView!.loopMode = .loop
        animationView!.animationSpeed = 0.5
        animationView!.play()
    }
    
    func binderViewModel(){
        self.mViewModel.refreshData = {[weak self] (code) in
            switch(code){
            case .getFolioOrder:
                self?.onGetFolioEnded()
            case .errorResponse:
                self?.onErrorResponse()
            case .createOrder:
                self?.onCreateEnded()
            default:
                self?.onErrorResponse()
            }
        }
    }
    
    @IBAction func GoToOrder(){
        let vc = CheckoutViewController()
        vc.idOrder = self.idOrder
        let backItem = UIBarButtonItem()
            backItem.title = ""
        self.navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func close(){
        self.dismiss(animated: true){
            self.checkoutDelegate.close()
        }
    }
    
    func setDataModel(folio:String){
        let today = Date()
        let methodPay = MethodPayModel(nTipoPago: 1, pagaCon: 0)
        let playerID = UtilClass.getDeviceId()
        let orders = mViewModel.getOrder(idOrder: idOrder)
        let totalPrice = getTotalCart(orders: orders)
        let products = productsPrepare(orders: orders)
        
        guard  let locationModel = UserPreferencesModel.shared.getLocationModel(),
        let restaurantModel = UserPreferencesModel.shared.getBranchModel()
        else{return}
        
        
        mViewModel.setFolio(data: folio)
        mViewModel.setPedido(data: PedidoModel())
        
        mViewModel.setIsInvited(data: true)
        mViewModel.setIsDelivery(data: true)
        mViewModel.setDeliveryPrice(data: 0.0)
        mViewModel.setDate(data: today.description)
        mViewModel.setMethodPay(data: methodPay)
        mViewModel.setIndex(data: 0)
        mViewModel.setDrinks(data: [])
        mViewModel.setAddOns(data: [])
        mViewModel.setPromos(data: [])
        mViewModel.setIdCategories(data: 0)
        mViewModel.setIdCoupon(data: "")
        mViewModel.setPlayerId(data: playerID )
        mViewModel.setstatusDelivery(data: 0)
        mViewModel.setBuyType(data: 1)
        mViewModel.setRegDelivery(data: locationModel._id)
        mViewModel.setIdRest(data: restaurantModel.nIdRestaurante)
        mViewModel.setIdSucursal(data: restaurantModel._id)
        mViewModel.setRestName(data: restaurantModel.cNombreRestaurante)
        mViewModel.setNameExt(data: restaurantModel.cNombreSucursal)
        mViewModel.setSubTotal(data: totalPrice)
        mViewModel.setTotal(data: totalPrice)
        mViewModel.setOrders(data: products)
        mViewModel.setUser(data:self.userForm)
        
        mViewModel.createOrder(.createOrder)
    }
    func getTotalCart(orders:[OrderItemModel])-> Double{
        var result = 0.0
        orders.enumerated().forEach{ index, it in
            result += it.total ?? 0.0
        }
        return result
    }
    func productsPrepare(orders:[OrderItemModel]) -> [PlatillosModel]{
        var result:[PlatillosModel] = []
        orders.forEach{it in
            guard let product = ProductsLocalmanager.shared.getItemById(id: it.idProduct)
            else{return}
            
            let item = PlatillosModel(_id: product._id, bFoto: false, bIngredienteElegir: false, bPlatilloHome: false, cDescripcion: product.cDescripcion ?? "" , cFoto: product.cFoto, cNombrePlatillo: product.cNombrePlatillo, comentarios: it.notes ?? "" , lstComplementos:[], lstIngredientes: ["---"], lstPreparacion: product.lstPreparacion ?? [], lstTamanios: it.sizes, nIdCategoria: product.nIdCategoria , nIdRestaurante: product.nIdRestaurante, nIngredientes: 0, nPrecio: it.total ?? 0.0, preparacion: [], quantity: it.quantity ?? 0)
            result.append(item)
        }
        return result
    }
    func onGetFolioEnded(){
        guard let folio = mViewModel.getFolioResponse else{return}
        mViewModel.initModel(data: CreateOrderModel())
        setDataModel(folio: folio)
    }
    
    func onCreateEnded(){
        guard let order = mViewModel.createOrderResponse else{return}
        print(order)
    }
    
    func onErrorResponse(){
        guard let error = mViewModel.errorResponse else{return}
        print("ERROR: \(String(describing: error.message))")
    }
}
