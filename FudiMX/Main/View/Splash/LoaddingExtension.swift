//
//  LoaddingExtension.swift
//  FudiMX
//
//  Created by Cristian Canedo on 26/05/22.
//

import Foundation

extension LoaddingViewController: RetryConnection{
    func reConnected() {
        self.getData()
    }
}
