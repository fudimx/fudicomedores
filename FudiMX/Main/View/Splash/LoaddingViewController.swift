//
//  LoaddingViewController.swift
//  FudiMX
//
//  Created by Cristian Canedo on 09/04/22.
//

import UIKit
import Lottie

class LoaddingViewController: UIViewController {

    @IBOutlet weak var animationView:AnimationView!

    let mViewModel = SplashViewModel()
    let alert = JojoAlert()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        clearData()
        lottieSetup()
        binderViewModel()
        getData()
    }
    
    func clearData(){
        LocationLocalManager.shared.deleteAll()
        BranchLocalManage.shared.deleteAll()
        MenuLocalManager.shared.deleteAll()
        ProductsLocalmanager.shared.deleteAll()
        PoligonRegionLocalModel.shared.deleteAll()
        SizeLocalManager.shared.deleteAll()
        SizeCartLocalManage.shared.deleteAll()
        AddOnsLocalManage.shared.deleteAll()
    }
    func binderViewModel(){
        mViewModel.refreshData = {[weak self] (code) in
            switch(code){
            case .getLocationsRequest:
                self?.onGetlocations()
            case .saveLocation:
                self?.onSaveLocationResponse()
            case .errorResponse:
                self?.onErrorResponse()
            default:
                self?.onErrorResponse()
                break
            }
        }
    }
    func getData(){
        mViewModel.getLocationsRequest(.getLocationsRequest)
    }
    func onGetlocations(){
        let locationsArray = mViewModel.getLocationsResponse
        mViewModel.saveLocations(.saveLocation, data: locationsArray)
    }
    func onErrorResponse(){
        guard let error = self.mViewModel.errorResponse else{return}
        if(error.codeError != 0){
            let msg = error.messageError(msg: error.message ?? "")
            alert.showBottomSheet(on: self, whit: msg, typeAlert: .Error, selector: nil)
        }else{
            let vc = NoInternetViewController()
            vc.isModalInPresentation = true
            vc.delegateRetry = self
            self.present(vc, animated: true)
        }
    }
    func onSaveLocationResponse(){
        guard let saveRes = mViewModel.saveLocationsResponse else{return}
        if saveRes{
            mViewModel.savePoligons()
            _ = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(fireTimer), userInfo: nil, repeats: false)
        }else{
            alert.showBottomSheet(on: self, whit: "Error al obtener y guardar plazas, porfavor intente de nuevo.", typeAlert: .Error, selector: nil)
        }
    }
    
    @objc func fireTimer(){
        let window = UIApplication.shared.keyWindow
        let vc = WelcomeViewController()
        vc.modalPresentationStyle = .fullScreen
        if let w = window {
            w.rootViewController = vc
            window?.makeKeyAndVisible()
        }
    }
    
    func lottieSetup(){
        animationView!.frame = view.bounds
        animationView!.contentMode = .scaleAspectFit
        animationView!.loopMode = .loop
        animationView!.animationSpeed = 0.5
        animationView!.play()
    }
}
