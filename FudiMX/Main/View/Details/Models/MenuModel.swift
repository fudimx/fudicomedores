//
//  MenuModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 07/04/22.
//

import Foundation

struct MenuModel:Codable{
    var title:String = ""
    var isSelected:Bool = false
    var products:[ProductsModel] = []
    
    var desc:String?
    var id:String?
    var icon:String?
    var idBranch:String = ""
    
    init(_ id:String, title text:String,_ value:Bool,_ array:[ProductsModel], description value2:String, icon value3:String,_ idBranch:String){
        self.title = text
        self.isSelected = value
        self.products = array
        
        self.desc = value2
        self.id = id
        self.icon = value3
        self.idBranch = idBranch
    }
}
struct ProductsModel:Codable{
    var title:String = ""
    var description:String = ""
    var price:Double = 0.0
    var id:String = ""
    var type:Int = 0
    var idMenu:String
    
    init(id:String, title text:String, description:String, price:Double, type:Int, idMenu:String){
        self.title = text
        self.description = description
        self.price = price
        self.type = type
        self.idMenu = idMenu
        self.id = id
    }
}
