//
//  DetailsViewController.swift
//  FudiMX
//
//  Created by Cristian Canedo on 07/04/22.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var heigthFrame:NSLayoutConstraint!
    @IBOutlet weak var frameView:UIView!
    @IBOutlet weak var tv:UITableView!
    @IBOutlet weak var cv:UICollectionView!
    
    @IBOutlet weak var CartCount:UILabel!
    @IBOutlet weak var CartTotal:UILabel!
    @IBOutlet weak var titleBranchLabel:UILabel!
    @IBOutlet weak var descriptionBranchLabel:UILabel!
    @IBOutlet weak var imageBranchBanner:UIImageView!

    let refreshControl = UIRefreshControl()
    let mViewmodel = DetailsViewModel()
    let alert = JojoAlert()
    var delegate:HomeDelegate!
    
    var branchModel:BranchModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initCollectionView()
        setupTableView()
        binderViewModel()
        initUi()
    }
    func initUi(){
        titleBranchLabel.text =  UtilClass.RealtionCatToEmoji(desc:branchModel.nIdCategoria)
        descriptionBranchLabel.text = branchModel.cNombreSucursal
        let nameImage = UtilClass.RealtionCatToBackGround(desc: branchModel.nIdCategoria)
        imageBranchBanner.image = UIImage(named: nameImage)
        navigationController?.setNavigationBarHidden(false, animated: true);
        refreshControl.attributedTitle = NSAttributedString(string: "Actualizar")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tv.addSubview(refreshControl)
        self.mViewmodel.getMenu(.getMenu, idBranch: self.mViewmodel.idBranch)
    }
    override func viewWillAppear(_ animated: Bool) {
        guard let navView = self.navigationController else {return}
        navView.navigationBar.prefersLargeTitles = false
        checkShoppingCart()
    }
    func binderViewModel(){
        self.mViewmodel.refreshData = {[weak self] (code) in
            switch(code){
            case .getMenuRequest:
                self?.onGetMenuRequestEnded()
            case .saveMenu:
                self?.onSaveMenuEnded()
            case .getMenu :
                self?.onGetMenuEnded()
            case .getProducts:
                self?.onGetProductsEnded()
            case .deleteShoppingCart:
                self?.onDeleteShoppingCart()
            case .errorResponse:
                self?.onErrorResponse()
            default:
                break
            }
        }
    }
    func initAnimation(constant:CGFloat){
        self.heigthFrame.constant = constant
        UIView.animate(withDuration: 0.5, delay:0) {
            self.view.layoutIfNeeded()
        }
    }
    func setupTableView(){
        let nib = UINib(nibName: "TableViewCell", bundle: nil)
        tv.register(nib, forCellReuseIdentifier: TableViewCell.identifier)
    }
    func checkShoppingCart(){
        guard let watchCart = mViewmodel.getShoppingCartActive() else{
            CartCount.text = "0"
            CartTotal.text = "$0.0"
            initAnimation(constant:0)
            return
        }
        CartCount.text = "\(watchCart.totalItems ?? 0)"
        CartTotal.text = "$\(watchCart.totalPay ?? 0.0)"
        initAnimation(constant:90)
    }
    func initCollectionView(){
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        cv.collectionViewLayout = layout
        cv.register(MenuDetailsCollectionViewCell.nib(), forCellWithReuseIdentifier: MenuDetailsCollectionViewCell.identifier)
       
    }
    func onGetMenuRequestEnded(){
        mViewmodel.saveMenu(.saveMenu)
    }
    func onSaveMenuEnded(){
        mViewmodel.getMenu(.getMenu, idBranch: self.mViewmodel.idBranch)
    }
    func onGetMenuEnded(){
        cv.reloadData()
        tv.reloadData()
    }
    func onGetProductsEnded(){
        tv.reloadData()
    }
    func onDeleteShoppingCart(){
        guard let response = mViewmodel.deleteCartResponse else{return}
        if response{
            checkShoppingCart()
        }
    }
    func onErrorResponse(){
        guard let error = mViewmodel.errorResponse else{return}
        let msg = error.messageError(msg: error.message ?? "")
        alert.showBottomSheet(on: self, whit: msg, typeAlert: .Error, selector: nil)
    }
    @objc func refresh(_ sender: AnyObject) {
        mViewmodel.getInitialMenu(.getMenuRequest, idBranch: self.mViewmodel.idBranch)
        refreshControl.endRefreshing()
    }
    
    @IBAction func searchAction(_ sender:UIButton){
        let vc = SearchViewController()
        vc.delegate = self
        vc.firstTableArray = self.mViewmodel.getProductsList(idBranch:self.mViewmodel.idBranch)
        present(vc, animated: true)
    }
    @IBAction func payAction(_ sender:UIButton){
        let vc = ShoppingCartViewController()
        vc.delegate = self
        vc.delegateChackout = self
        let navVc = UINavigationController(rootViewController: vc )
        navVc.isModalInPresentation = true
        navVc.navigationBar.tintColor = .black
        present(navVc, animated: true)
    }
    @IBAction func deleteCartAction(_ sender:UIButton){
        mViewmodel.deleteCart(.deleteShoppingCart)
    }
}
