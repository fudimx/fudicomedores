//
//  TableViewCell.swift
//  FudiMX
//
//  Created by Cristian Canedo on 07/04/22.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblDescription:UILabel!
    @IBOutlet weak var lblPrice:UILabel!
    
    static let identifier = "tableViewCell"
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
