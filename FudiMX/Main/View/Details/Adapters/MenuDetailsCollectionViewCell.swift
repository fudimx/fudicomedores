//
//  MenuDetailsCollectionViewCell.swift
//  FudiMX
//
//  Created by Cristian Canedo on 07/04/22.
//

import UIKit

class MenuDetailsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var indicatorView:UIView!
    
    static let identifier = "menuDetailsViewCell"
    static func nib() -> UINib{
        return UINib(nibName: "MenuDetailsCollectionViewCell", bundle: nil)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
