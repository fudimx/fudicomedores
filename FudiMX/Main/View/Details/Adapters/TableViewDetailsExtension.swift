//
//  TableViewDetailsExtension.swift
//  FudiMX
//
//  Created by Cristian Canedo on 07/04/22.
//

import Foundation
import UIKit

extension DetailsViewController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mViewmodel.getProductsResponse.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.mViewmodel.getProductsResponse[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.identifier, for: indexPath) as! TableViewCell
        let price = item.lstTamanios.isEmpty ? 0.0:item.lstTamanios[0].nPrecio
        cell.lblPrice.text = "$\(price)"
        cell.lblTitle.text = item.cNombrePlatillo
        cell.lblDescription.text = item.cDescripcion
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = ProductDetailsViewController()
        vc.data = self.mViewmodel.getProductsResponse[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension DetailsViewController:UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mViewmodel.getMenuResponse.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = collectionView.dequeueReusableCell(withReuseIdentifier: MenuDetailsCollectionViewCell.identifier, for: indexPath) as! MenuDetailsCollectionViewCell
        let it = self.mViewmodel.getMenuResponse[indexPath.row]
        item.lblTitle.text = it.cDescripcion
        if(it.isSelected ?? false){
            item.indicatorView.backgroundColor = .label
        }else{
            item.indicatorView.backgroundColor = .systemBackground
        }
        return item
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSize = self.mViewmodel.getMenuResponse[indexPath.item].cDescripcion ?? ""
        return CGSize(width: (cellSize.stringWidth * 1.3) + 16 , height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.clearCollection()
        self.mViewmodel.getMenuResponse[indexPath.item].isSelected = true
        self.mViewmodel.getProductsByMenu(.getProducts, index: indexPath.row)
        collectionView.reloadData()
    }
    func clearCollection(){
        self.mViewmodel.getMenuResponse.enumerated().forEach{ (index, it) in
            self.mViewmodel.getMenuResponse[index].isSelected = false
        }
    }
   
}
extension DetailsViewController: SearchControllerDelegate{
    func onItemSelected(item: ProductsModel?) {
        let vc = ProductDetailsViewController()
        vc.data = item
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension DetailsViewController: DetailsDelegate, CheckoutDelegate{
    func onDeleteCart() {
        self.checkShoppingCart()
    }
    func close() {
        self.navigationController?.popViewController(animated: false)
        self.delegate.onCheckoutSuccess()
    }
}
