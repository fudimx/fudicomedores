//
//  NoInternetViewController.swift
//  FudiMX
//
//  Created by Cristian Canedo on 25/05/22.
//

import UIKit
import Lottie

class NoInternetViewController: UIViewController {

    @IBOutlet weak var animationView:AnimationView!
    var delegateRetry:RetryConnection?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lottieSetup()
    }
    @IBAction func checkInternetStatus(_ sender:UIButton){
        self.dismiss(animated: true) {
            guard let delegate = self.delegateRetry else {
                return
            }
            delegate.reConnected()
        }
    }
    func lottieSetup(){
        animationView!.frame = view.bounds
        animationView!.contentMode = .scaleAspectFit
        animationView!.loopMode = .loop
        animationView!.animationSpeed = 0.5
        animationView!.play()
    }
}
