//
//  WelcomeExtension.swift
//  FudiMX
//
//  Created by Cristian Canedo on 09/04/22.
//

import Foundation
import UIKit

extension WelcomeViewController:WelcomeControllerDelegate{
    func onLocationSelected(item: LocationModel?) {
        guard let it = item else{
            ManagerAlerts.simpleAlert(on: self, title: "¡Espera!", description: "No pudimos localizar la plaza porfavr intenta de nuevo.", action:{})
            return
        }
        mviewModel.saveLocationSelected(model: it)
        mviewModel.getBranches(.getBranchesRequest, idLocation: it._id)
    }
    
    func nextStep(){
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "navController") as! UINavigationController
        viewController.modalPresentationStyle = .fullScreen
        self.present(viewController, animated: true, completion: nil)
    }
    
}
