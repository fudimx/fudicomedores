//
//  WelcomeViewController.swift
//  FudiMX
//
//  Created by Cristian Canedo on 09/04/22.
//

import UIKit
import Lottie

class WelcomeViewController: UIViewController {

    @IBOutlet weak var animationView:AnimationView!

    let mviewModel = WelcomeViewModel()
    let alert = JojoAlert()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lottieSetup()
        binderViewModel()
    }

    func binderViewModel(){
        mviewModel.refreshData = {[weak self] (code) in
            switch(code){
            case .getBranchesRequest:
                self?.onGetBranchesReq()
            case .saveBranch:
                self?.onSaveBranch()
            case .errorResponse:
                self?.errorResponse()
            default:
                break
            }
        }
    }
    func onSaveBranch(){
        guard let isSave = self.mviewModel.saveBranchsResponse else{ return }
        if isSave{
            self.nextStep()
        }
    }
    func onGetBranchesReq(){
        let branches = self.mviewModel.getBranchesRequestResponse
        self.mviewModel.saveBranch(.saveBranch, data: branches)
    }
    func errorResponse(){
        guard let error = self.mviewModel.errorResponse, let msg = error.message else{return}
        alert.showBottomSheet(on: self, whit: error.messageError(msg: msg), typeAlert: .Error, selector: nil)
    }
    @IBAction func scannQr(_ sender:UIButton){
        let presenter = ScannCodeViewController()
        presenter.isModalInPresentation = true
        presenter.delegate = self
        self.present(presenter, animated: true)
    }
    @IBAction func showLocations(_ sender:UIButton){
        let presenter = LocationsViewController()
        presenter.isModalInPresentation = true
        presenter.delegateIn = self
        self.present(presenter, animated: true)
    }
    func lottieSetup(){
        animationView!.frame = view.bounds
        animationView!.contentMode = .scaleAspectFit
        animationView!.loopMode = .loop
        animationView!.animationSpeed = 0.5
        //view.addSubview(animationView!)
        animationView!.play()
    }
}
