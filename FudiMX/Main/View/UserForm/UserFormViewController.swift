//
//  UserFormViewController.swift
//  FudiMX
//
//  Created by Cristian Canedo on 09/08/22.
//

import UIKit

class UserFormViewController: UIViewController, UISheetPresentationControllerDelegate {
    
    @IBOutlet weak var txtNameUser:UITextField!
    @IBOutlet weak var txtPhoneUser:UITextField!
    @IBOutlet weak var txtPayment:UITextField!

    var formUserDelegate:UserFormDelegate!
    var totalToPay = 0.0
    let alert = JojoAlert()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        if #available(iOS 15.0, *) {
            sheetPresentationController?.delegate = self
            sheetPresentationController?.selectedDetentIdentifier = .medium
            sheetPresentationController?.prefersGrabberVisible = true
            sheetPresentationController?.detents = [.medium()]
        }
        
        txtPayment.inputAccessoryView = UtilClass.addToolbar(on: self, action: #selector(self.closeKeyboard), titleButton: "Cerrar")
        txtPhoneUser.inputAccessoryView = UtilClass.addToolbar(on: self, action: #selector(self.closeKeyboard), titleButton: "Cerrar")
        self.initData()
    }
    private func initData(){
        if let info = UserPreferencesModel.shared.getUserinfoModel(){
            txtPhoneUser.text = info.cTelefono
            txtNameUser.text = info.cNombres
        }
    }
    @objc func closeKeyboard(){
        self.view.endEditing(true)
    }
    @IBAction func saveData(_ sender:UIButton){
        guard let name = txtNameUser.text,  let phone = txtPhoneUser.text else{
            alert.showBottomSheet(on: self, whit: "Todos los datos son obligatorios.", typeAlert: .Info, buttonTitle: "Continuar", selector:nil)
            return
        }
        if phone.count != 10{
            alert.showBottomSheet(on: self, whit: "El número de telefono es a 10 caracteres.", typeAlert: .Info, buttonTitle: "Continuar", selector:nil)
        }else{
            if(name != "" && phone != ""){
                let data = UserPedidoModel(cNombres: name, cTelefono: phone)
                self.dismiss(animated: true) {
                    UserPreferencesModel.shared.initUserInfo(data: data)
                    self.formUserDelegate.saveUser(data: data, payment: "0")
                }
            }else{
                alert.showBottomSheet(on: self, whit: "Todos los datos son obligatorios.", typeAlert: .Info, buttonTitle: "Continuar", selector:nil)
            }
        }
        
    }
    @IBAction func close(_ sender:UIButton){
        self.dismiss(animated: true)
    }
}
extension UserFormViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == txtNameUser){
            txtPhoneUser.becomeFirstResponder()
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let str = "\(textField.text!)\(string)"
        
        switch textField{
        case txtPhoneUser:
            return str.count <= 10
        default:
            return true
        }
    }
}
