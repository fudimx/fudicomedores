//
//  LocationsViewController.swift
//  FudiMX
//
//  Created by Cristian Canedo on 09/04/22.
//

import UIKit

class LocationsViewController: UIViewController {

    @IBOutlet weak var tvList:UITableView!
    
    let mviewModel = LocationsViewModel()
    var delegateIn:WelcomeControllerDelegate?
    override func viewDidLoad(){
        super.viewDidLoad()
        setupTableView()
        binderViewModel()
        mviewModel.getlocations(.getLocations)
    }
    func binderViewModel(){
        mviewModel.refreshData = {[weak self] (code) in
            switch(code){
            case .getLocations:
                self?.onGetLocationsEnded()
            default:
                break
            }
        }
    }
    func setupTableView(){
        let nib = UINib(nibName: "LocationsTableViewCell", bundle: nil)
        tvList.register(nib, forCellReuseIdentifier: LocationsTableViewCell.identifier)
    }
    func onGetLocationsEnded(){
        tvList.reloadData()
    }
    @IBAction func close(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
}
