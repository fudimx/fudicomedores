//
//  LocationsTableViewExtension.swift
//  FudiMX
//
//  Created by Cristian Canedo on 09/04/22.
//

import Foundation
import UIKit

extension LocationsViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mviewModel.getLocationsResponse.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: LocationsTableViewCell.identifier, for: indexPath) as! LocationsTableViewCell
        
        let item = mviewModel.getLocationsResponse[indexPath.row]
        cell.lblTitle.text = item.nombreRegion
        cell.lblAddress.text = item.ubicacion
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 97.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let delegate = self.delegateIn else{return}
        let locModel = mviewModel.getLocationsResponse[indexPath.row]
        self.dismiss(animated: true) {
            delegate.onLocationSelected(item: locModel)
        }
    }
}
