//
//  LocationsTableViewCell.swift
//  FudiMX
//
//  Created by Cristian Canedo on 09/04/22.
//

import UIKit

class LocationsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblAddress:UILabel!

    static let identifier = "locationsTableViewCell"
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
