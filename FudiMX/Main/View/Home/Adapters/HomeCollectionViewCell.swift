//
//  HomeCollectionViewCell.swift
//  FudiMX
//
//  Created by Cristian Canedo on 05/04/22.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblDescription:UILabel!
    @IBOutlet weak var imgBanner:UIImageView!
    
    static let identifier = "homeItem"
    static func nib() -> UINib{
        return UINib(nibName: "HomeCollectionViewCell", bundle: nil)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
