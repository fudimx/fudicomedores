//
//  HomeHeaderCollectionReusableView.swift
//  FudiMX
//
//  Created by Cristian Canedo on 05/04/22.
//

import UIKit

class HomeHeaderCollectionReusableView: UICollectionReusableView, UIScrollViewDelegate{
    
    @IBOutlet weak var headderContainer:UIView!
    
    static let identifier = "HomeHeaderReusableView"
    let mViewModel = HeaderHomeViewmodel()
    
    static func nib() -> UINib{
        return UINib(nibName: "HomeHeaderCollectionReusableView", bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
