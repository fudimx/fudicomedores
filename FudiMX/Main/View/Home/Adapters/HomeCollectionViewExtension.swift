//
//  HomeCollectionViewExtension.swift
//  FudiMX
//
//  Created by Cristian Canedo on 05/04/22.
//

import Foundation
import UIKit

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mViewmodel.getBranches.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCollectionViewCell.identifier, for: indexPath) as! HomeCollectionViewCell
        let item = self.mViewmodel.getBranches[indexPath.row]
        let imageName = UtilClass.RealtionCatToBackGround(desc:item.nIdCategoria)
        cell.lblTitle.text = item.cNombreRestaurante
        cell.lblDescription.text = UtilClass.RealtionCatToEmoji(desc:item.nIdCategoria)
        cell.imgBanner.image = UIImage(named: imageName)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 8, left: 10, bottom: 0, right: 10)
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: HomeHeaderCollectionReusableView.identifier, for: indexPath) as! HomeHeaderCollectionReusableView
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.clickHeadder))
        header.headderContainer.addGestureRecognizer(gesture)
        return header
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let heigth = mViewmodel.haveDrinks >= 0 ? 250.0 : 0.0
        return CGSize(width:self.view.frame.width, height: heigth)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.gotoDetails(row:indexPath.row, false)
    }
    @objc func clickHeadder(){
        if mViewmodel.haveDrinks >= 0{
            self.gotoDetails(row: self.mViewmodel.haveDrinks, true)
        }
    }
    func gotoDetails(row:Int,_ spetialItem:Bool){
        guard let navView = self.navigationController else {return}
        let vc = DetailsViewController()
        var branch = self.mViewmodel.getBranches[row]
        if spetialItem{
            branch.nIdCategoria = 11
        }
        UserPreferencesModel.shared.iniBranchModel(data: branch)
        vc.mViewmodel.idBranch = branch.nIdRestaurante
        vc.delegate = self
        vc.branchModel = branch
        
        navView.pushViewController(vc, animated: true)
    }
}

extension ViewController:HomeDelegate{
    func onCheckoutSuccess() {
        self.dismiss(animated: true)
    }
}
