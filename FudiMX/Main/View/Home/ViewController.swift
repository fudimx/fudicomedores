//
//  ViewController.swift
//  FudiMX
//
//  Created by Cristian Canedo on 28/03/22.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var cv:UICollectionView!
    @IBOutlet weak var lblTitleLocation:UILabel!
    
    let mViewmodel = HomeViewModel()
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Inicio"
        initCollectionView()
        bindViewModel()
        let locationModel = UserPreferencesModel.shared.getLocationModel()
        mViewmodel.getBrnaches(.getBranch, idLocation: locationModel?._id ?? "")
        lblTitleLocation.text = locationModel?.nombreRegion ?? ""
    }
    
    func bindViewModel(){
        mViewmodel.refreshData = {[weak self] (code) in
            switch(code){
            case .getBranch:
                self?.onGetBranchesended()
            default:
                break
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        guard let navView = self.navigationController else {return}
        navView.setNavigationBarHidden(true, animated: true);
    }
    
    func initCollectionView(){
        
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        cv.addSubview(refreshControl)
        cv.alwaysBounceVertical = true
        
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: view.frame.size.width/2.2, height: 250)
        cv.collectionViewLayout = layout
        cv.register(HomeCollectionViewCell.nib(), forCellWithReuseIdentifier: HomeCollectionViewCell.identifier)
        cv.register(HomeHeaderCollectionReusableView.nib(), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: HomeHeaderCollectionReusableView.identifier)
    }
    func onGetBranchesended(){
        cv.reloadData()
    }
    @objc func refresh(){
        refreshControl.endRefreshing()
    }
    @IBAction func closeWindow(){
        self.dismiss(animated: true, completion: nil)
    }
}

