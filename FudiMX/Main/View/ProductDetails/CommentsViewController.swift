//
//  CommentsViewController.swift
//  FudiMX
//
//  Created by Cristian Canedo on 08/04/22.
//

import UIKit

class CommentsViewController: UIViewController, UISheetPresentationControllerDelegate{

    @IBOutlet weak var txtComments:UITextField!
    
    var delegate:ProductDetailsDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtComments.delegate = self
        txtComments.becomeFirstResponder()
        self.title = "Direcciones"
        if #available(iOS 15.0, *) {
            sheetPresentationController?.delegate = self
            sheetPresentationController?.selectedDetentIdentifier = .medium
            sheetPresentationController?.prefersGrabberVisible = true
            sheetPresentationController?.detents = [.medium()]
        }
    }

}
