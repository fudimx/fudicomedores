//
//  CommentExtension.swift
//  FudiMX
//
//  Created by Cristian Canedo on 20/04/22.
//

import Foundation
import UIKit

extension CommentsViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let note = self.txtComments.text!
        self.delegate?.onWriteNote(note: note)
        self.dismiss(animated: true)
        return true
    }
}
