//
//  ProductDetailsExtension.swift
//  FudiMX
//
//  Created by Cristian Canedo on 20/04/22.
//

import Foundation

extension ProductDetailsViewController: ProductDetailsDelegate{
    func onAddOnsSelected(addOns: [String]) {
        self.mViewModel.addOns = addOns
    }
    func onWriteNote(note: String) {
        self.mViewModel.notes = note
    }
}
