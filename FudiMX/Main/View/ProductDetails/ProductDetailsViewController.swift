//
//  ProductDetailsViewController.swift
//  FudiMX
//
//  Created by Cristian Canedo on 08/04/22.
//

import UIKit
import AlamofireImage

class ProductDetailsViewController: UIViewController {

    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblPrice:UILabel!
    @IBOutlet weak var lblDescription:UILabel!
    @IBOutlet weak var lblCountProduct:UILabel!
    @IBOutlet weak var stackSizes:UIStackView!
    @IBOutlet weak var imgBanner:UIImageView!
    @IBOutlet weak var heiggthImage:NSLayoutConstraint!
    
    let mViewModel = ProductDetailViewModel()
    var data:ProductsModel?
    let alert = JojoAlert()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initData()
        sizes()
        binderViewModel()
        self.title = ""
    }
 
    func initData(){
        guard let dto = data else{return}
        
        let dtoSize = dto.lstTamanios.isEmpty ? 0.0 : dto.lstTamanios[0].nPrecio
        lblTitle.text = dto.cNombrePlatillo
        lblPrice.text = "$\(dtoSize)"
        lblDescription.text = dto.cDescripcion
        
        stackSizes.spacing = 10
        
        
        if dto.cFoto != ""{
            heiggthImage.constant = 200
            let urlImage = URL(string: "https://drive.google.com/uc?export=download&id=" + dto.cFoto)!
            imgBanner.af.setImage(withURL: urlImage)
        }else{
            heiggthImage.constant = 0
        }
    }
    func sizes(){
        guard let tamaniosData = data else{return}
        if(tamaniosData.lstTamanios.count > 1){
            var HStack:UIStackView!
            tamaniosData.lstTamanios.enumerated().forEach{ index, it in
                if (index%2) == 0{
                    HStack = UIStackView()
                    HStack.alignment = .fill
                    HStack.distribution = .fillEqually
                    HStack.axis = .horizontal
                    HStack.spacing = 10
                }
                
                let buttonSize = UIButton()
                buttonSize.setTitle(it.cNombreTamanio, for: .normal)
                buttonSize.backgroundColor = .systemGray6
                buttonSize.setTitleColor(.label, for: .normal)
                buttonSize.cornerRadius = 10
                buttonSize.titleLabel?.font = UIFont.systemFont(ofSize: 14.0, weight: .medium)
                buttonSize.tag = index
                buttonSize.addTarget(self, action: #selector(self.selectSizeAction(_:)), for: .touchUpInside)
                
                HStack.addArrangedSubview(buttonSize)
                if (index%2) == 0{
                    stackSizes.addArrangedSubview(HStack)
                }
            }
        }else if(tamaniosData.lstTamanios.count == 1){
            let emptylabel = UILabel(frame: CGRect(x: 0, y: 0, width: 0, height: 45))
            stackSizes.addArrangedSubview(emptylabel)
            mViewModel.sizeSelected = tamaniosData.lstTamanios[0]
        }
    }
    func binderViewModel(){
        self.mViewModel.refreshData = {[weak self] (code) in
            switch(code){
            case .saveShoppingCart:
                self?.onSaveCartItem()
            case .setAddOns:
                self?.onSetAddOns()
            case .deleteShoppingCart:
                self?.onDeleteCart()
            case .getAddOns:
                self?.onGetAddOnResponse()
            default:
                break
            }
        }
    }
    @IBAction func addCommentAction(_ sender:UIButton){
        if #available(iOS 15.0, *) {
            let vc = CommentsViewController()
            vc.delegate = self
            present(vc, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Comentarios", message: "ingresa un comentario.", preferredStyle: .alert)

            alert.addTextField { (textField) in
                textField.placeholder = ""
            }

            alert.addAction(UIAlertAction(title: "Enviar", style: .default, handler: { [weak alert] (_) in
                let textField = alert?.textFields![0]
                let text = textField?.text ?? ""
                self.mViewModel.notes = text
            }))
            alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel))

            self.present(alert, animated: true, completion: nil)
        }
    }
    @IBAction func addLessButtons(_ sender:UIButton){
        switch(sender.tag){
        case 0:
            if(mViewModel.numProducts > 1){
                mViewModel.numProducts -= 1
            }
        case 1:
            if(mViewModel.numProducts < 10){
                mViewModel.numProducts += 1
            }
        default: break
        }
        lblCountProduct.text = "\(mViewModel.numProducts)"
    }
    @IBAction func addToCartAction(_ sender:UIButton){
        guard mViewModel.sizeSelected != nil else{
            alert.showBottomSheet(on: self, whit: "Selecciona un tamaño.", typeAlert: .Warning, selector: nil)
            return
        }
        
        guard let branchSelected = UserPreferencesModel.shared.getBranchModel() else{
            alert.showBottomSheet(on: self, whit: "Error al obtener datos, intente de nuevo.", typeAlert: .Info, buttonTitle: "Continuar", selector: #selector(self.clearCart))
            return
        }
        let branchCart = UserPreferencesModel.shared.getBranchCartModel()
        if(branchSelected.nIdRestaurante == branchCart?.nIdRestaurante ?? branchSelected.nIdRestaurante){
            guard let idData = data else{return}
            self.mViewModel.getAddOns(.getAddOns, idProduct: idData._id)
        }else{
            alert.showBottomSheet(on: self, whit: "Estas tratando de agregar un pedido de otro restaurante, los productos del carrito se eliminaran y agregaran estos nuevos, ¿Estas de acuerdo?", typeAlert: .Info, buttonTitle: "Eliminar", selector: #selector(self.clearCart))
        }
    }
    func onGetAddOnResponse(){
        let result = mViewModel.addOndsResponse
        if result.count > 0 {
            openAddOns("onGetAddOnResponse")
        }else{
            mViewModel.addOns = []
        }
    }
    @objc func selectSizeAction(_ sender:UIButton){
        guard let product = data else{return}
        let item = product.lstTamanios[sender.tag]
        clearComponents()
        sender.backgroundColor = setColor(.PrimaryColor)
        sender.setTitleColor(.white, for: .normal)
        lblPrice.text = "$\(item.nPrecio)"
        mViewModel.sizeSelected = item
    }
    @objc func clearCart(){
        self.alert.dismissAlert()
        self.mViewModel.deleteCart(.deleteShoppingCart)
        openAddOns("clearCart")
    }
    func openAddOns(_ origen:String){
        print(origen)
        let vc = AddOnsViewController()
        vc.delegateAddons = self
        vc.arrayAddOns = mViewModel.addOndsResponse
        self.present(vc, animated: true)
    }
    func onSetAddOns(){
        guard let product = data else{return}

        let quantity = mViewModel.numProducts
        guard let dtoSelected = mViewModel.sizeSelected else{
            alert.showBottomSheet(on: self, whit: "Selecciona un tamaño.", typeAlert: .Warning)
            return
        }
        
        let price = product.lstTamanios.isEmpty ? 0.0 : dtoSelected.nPrecio
        let totalPrice = price * Double(mViewModel.numProducts)
        let notes = self.mViewModel.notes
        let szes = self.setSizesCart()
        let addOns = mViewModel.addOns
        let dataSave = ShoppingCartModel.init(id: UUID().uuidString , idProduct: product._id , notes: notes, quantity: Int(quantity), total: totalPrice, nameProduct: product.cNombrePlatillo, status: true, addOns: addOns, sizes: szes)
        mViewModel.addCart(.saveShoppingCart, data: dataSave)
    }
    func onDeleteCart(){
        let res = self.mViewModel.deleteCartResponse
        if res {
            openAddOns("onDeleteCart")
        }else{
            alert.showBottomSheet(on: self, whit: "Algo salio mal, intenta de nuevo", typeAlert: .Error)
        }
    }
    func clearComponents(){
        stackSizes.arrangedSubviews.forEach{ it in
            guard let secStack = it as? UIStackView else{return}
            secStack.arrangedSubviews.forEach{jt in
                guard let button = jt as? UIButton else{return}
                button.backgroundColor = .systemGray6
                button.setTitleColor(.label, for: .normal)
            }
        }
    }
    func onSaveCartItem(){
        let branchSelected = UserPreferencesModel.shared.getBranchModel()
        UserPreferencesModel.shared.iniBranchCartModel(data: branchSelected)
        alert.showBottomSheet(on: self, whit: "Producto agregado.", typeAlert: .Success, buttonTitle: "Aceptar", selector: #selector(self.actionAlert))
    }
    
    func setSizesCart() ->[SizeCartModel]{
        guard let dto = data else{return []}
        let sizes = dto.lstTamanios
        var arrayCartSizes:[SizeCartModel] = []
        sizes.forEach{it in
            let selected = it.cNombreTamanio == mViewModel.sizeSelected?.cNombreTamanio ? true :false
            let sizeCart = SizeCartModel(bSeleccionado: selected, cNombreTamanio: it.cNombreTamanio, nPrecio: it.nPrecio, quantity: it.quantity)
            arrayCartSizes.append(sizeCart)
        }
        return arrayCartSizes
    }
    @objc func actionAlert(){
        self.alert.dismissAlert()
        self.navigationController?.popViewController(animated: true)
    }
}
