//
//  CreateOrderRequest.swift
//  FudiMX
//
//  Created by Cristian Canedo on 23/06/22.
//

import Foundation
import Alamofire

private let timeoutInterval: Double = 30.0
private var configuration = Configuration()

private var dictionary:NSDictionary? {
    guard let path = Bundle.main.path(forResource: "Constants", ofType: "plist"), let dictionary = NSDictionary(contentsOfFile: path) else {
        return nil
    }
    return dictionary
}

func CreateOrderRequest(completion:@escaping(_ result:[LocationModel]) -> Void, failure:@escaping(_ error:ErrorResponse) -> Void){
    guard let pncgrpDic = dictionary else {return}
    let baseURL = configuration.environment.baseURL
    let pathDictionary = pncgrpDic.object(forKey: "paths") as! NSDictionary
    let path = pathDictionary.object(forKey: "createOrder") as! String
    let url = "\(baseURL)\(path)"
    let pncgrp = pncgrpDic.object(forKey: "picnic-group") as? String ?? ""
    
    let headder:HTTPHeaders = ["picnic-group":pncgrp]
    AF.request(url, method: .post, headers: headder){ urlRequest in
        urlRequest.timeoutInterval = timeoutInterval
    }.validate()
        .responseDecodable(of: BaseArrayModel<LocationModel>.self) { response in
        switch response.result {
        case .success:
            if let data = response.value{
                if data.bError {
                    let statusCode = response.response?.statusCode ?? 0
                    let error = ErrorResponse(message: "Error", codeError: statusCode)
                    failure(error)
                }else{
                    completion(data.data ?? [])
                }
            }
        case .failure(_):
            let statusCode = response.response?.statusCode ?? 0
            let error = ErrorResponse(message: "Error", codeError: statusCode)
            failure(error)

        }
    }
}

