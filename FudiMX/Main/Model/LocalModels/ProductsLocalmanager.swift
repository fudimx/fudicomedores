//
//  ProductsLocalmanager.swift
//  FudiMX
//
//  Created by Cristian Canedo on 16/04/22.
//

import Foundation
import CoreData
import UIKit


public class ProductsLocalmanager{
    
    private var ctxSession: NSManagedObjectContext = {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let contx = appDelegate.persistentContainer.viewContext
        return contx
    }()
    
    public static let shared: ProductsLocalmanager = {
            let instance = ProductsLocalmanager()
            return instance
    }()
    
    private func createModel(model:ProductsModel) -> Bool{
        guard let entity = NSEntityDescription.entity(forEntityName: "ProductsLDBModel", in: ctxSession) else{return false}
        
        if let modelExist = ifExist(id: model._id)  {
            return updateModel(entity: modelExist, model: model)
        }else{
            let res = ProductsLDBModel(entity: entity, insertInto: ctxSession)
            res.id = model._id
            res.nIdCategoria = model.nIdCategoria
            res.nIdRestaurante = model.nIdRestaurante
            res.cNombrePlatillo = model.cNombrePlatillo
            res.cDescripcion = model.cDescripcion
            res.cFoto = model.cFoto
            res.bActivo = model.bActivo
            res.bDisponible = model.bDisponible
            res.categoria = model.Categoria
        
            SizeLocalManager.shared.saveModels(models: model.lstTamanios, idProduct: model._id)
            AddOnsLocalManage.shared.saveModels(models: model.lstPreparacion ?? [], idProduct: model._id)

            do{
                try ctxSession.save()
                return true
            }catch{
                return false
            }
            
        }
    }
    
    func saveModels(models:[ProductsModel], response:@escaping(Bool) -> Void){
        var result = true
        for it in models{
           result = createModel(model: it) && result
        }
        response(result)
    }
    
    func updateModel(entity:ProductsLDBModel, model:ProductsModel) -> Bool{
        entity.nIdCategoria = model.nIdCategoria
        entity.nIdRestaurante = model.nIdRestaurante
        entity.cNombrePlatillo = model.cNombrePlatillo
        entity.cDescripcion = model.cDescripcion
        entity.cFoto = model.cFoto
        entity.bActivo = model.bActivo
        entity.bDisponible = model.bDisponible
        entity.categoria = model.Categoria
        SizeLocalManager.shared.saveModels(models: model.lstTamanios, idProduct: model._id)
        AddOnsLocalManage.shared.saveModels(models: model.lstPreparacion ?? [], idProduct: model._id)
            do{
                try ctxSession.save()
                return true
            }catch{
                return false
            }
    }
    func getItemById(id:String) -> ProductsModel?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ProductsLDBModel")
        request.predicate = NSPredicate(format: "id = %@", id)
        do{
            let fetching = try ctxSession.fetch(request).first
            let fetchConvert = fetching as? ProductsLDBModel
            let result = ProductsModel(_id: fetchConvert?.id ?? "" , nIdCategoria: fetchConvert?.nIdCategoria ?? "", nIdRestaurante: fetchConvert?.nIdRestaurante ?? "", cNombrePlatillo: fetchConvert?.cNombrePlatillo ?? "", cDescripcion: fetchConvert?.cDescripcion ?? "", cFoto: fetchConvert?.cFoto ?? "", bActivo: fetchConvert?.bActivo ?? false, bDisponible: fetchConvert?.bDisponible ?? false , Categoria: fetchConvert?.categoria ?? "")
            return result
        }catch{
            return nil
        }
    }
    func getModelsById(idMenu:String) -> [ProductsModel]?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ProductsLDBModel")
        request.predicate = NSPredicate(format: "nIdCategoria = %@", idMenu)
        
        do {
            let fetching = try ctxSession.fetch(request)
            var arrayResult:[ProductsModel] = []
            for item in fetching {
                let it = item as! ProductsLDBModel
                let sizes = SizeLocalManager.shared.getModels(idProduct: it.id ?? "")
                let addOns = AddOnsLocalManage.shared.getModels(idProduct: it.id ?? "")
                arrayResult.append(ProductsModel(_id: it.id ?? "", nIdCategoria: it.nIdCategoria ?? "", nIdRestaurante: it.nIdRestaurante ?? "", cNombrePlatillo: it.cNombrePlatillo ?? "", cDescripcion: it.cDescripcion ?? "", cFoto: it.cFoto ?? "", bActivo: it.bActivo, bDisponible: it.bDisponible, Categoria: it.categoria ?? "", lstTamanios: sizes ?? [], lstPreparacion: addOns ?? []))
            }
            return arrayResult
        } catch {
            return nil
        }
    }
    func getModelsByBranch(idBranch:String) -> [ProductsModel]?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ProductsLDBModel")
        request.predicate = NSPredicate(format: "nIdRestaurante = %@", idBranch)
        
        do {
            let fetching = try ctxSession.fetch(request)
            var arrayResult:[ProductsModel] = []
            for item in fetching {
                let it = item as! ProductsLDBModel
                let sizes = SizeLocalManager.shared.getModels(idProduct: it.id ?? "")
                let addOns = AddOnsLocalManage.shared.getModels(idProduct: it.id ?? "")
                
                arrayResult.append(ProductsModel(_id: it.id ?? "", nIdCategoria: it.nIdCategoria ?? "", nIdRestaurante: it.nIdRestaurante ?? "", cNombrePlatillo: it.cNombrePlatillo ?? "", cDescripcion: it.cDescripcion ?? "", cFoto: it.cFoto ?? "", bActivo: it.bActivo, bDisponible: it.bDisponible, Categoria: it.categoria ?? "", lstTamanios: sizes ?? [], lstPreparacion: addOns ?? []))
            }
            return arrayResult
        } catch {
            return nil
        }
    }
    
    private func ifExist(id:String) -> ProductsLDBModel?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ProductsLDBModel")
        request.predicate = NSPredicate(format: "id = %@", id)
        
        do {
            let fetching = try ctxSession.fetch(request).first
            return fetching as? ProductsLDBModel
        } catch {
            return nil
        }
    }
    
    func deleteAll(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ProductsLDBModel")
        do{
            let fetching = try ctxSession.fetch(request)
            for object in fetching {
                ctxSession.delete(object as! NSManagedObject)
            }
            try ctxSession.save()
        }catch{
        }
    }
}
