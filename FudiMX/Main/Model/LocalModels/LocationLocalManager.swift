//
//  LocationLocalManage.swift
//  FudiMX
//
//  Created by Cristian Canedo on 14/04/22.
//

import Foundation
import CoreData
import UIKit


public class LocationLocalManager{
    
    private var ctxSession: NSManagedObjectContext = {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let contx = appDelegate.persistentContainer.viewContext
        return contx
    }()
    
    public static let shared: LocationLocalManager = {
            let instance = LocationLocalManager()
            return instance
    }()
    
    private func createModel(model:LocationModel) -> Bool{
        guard let entity = NSEntityDescription.entity(forEntityName: "LocationLDBModel", in: ctxSession) else{return false}
        
        if let modelExist = ifExist(id: model._id)  {
            return updateModel(entity: modelExist, model: model)
        }else{
            let res = LocationLDBModel(entity: entity, insertInto: ctxSession)
            res.id = model._id
            res.ubicacion = model.ubicacion
            res.nombreRegion = model.nombreRegion
            res.plazaComercial = model.plazaComercial
            
            do{
                try ctxSession.save()
                return true
            }catch{
                return false
            }
            
        }
    }
    
    func saveModels(models:[LocationModel], response:@escaping(Bool) -> Void){
        var result = true
        for it in models{
           result = createModel(model: it) && result
        }
        response(result)
    }
    
    func updateModel(entity:LocationLDBModel, model:LocationModel) -> Bool{
        entity.plazaComercial = model.plazaComercial
        entity.nombreRegion = model.nombreRegion
        entity.ubicacion = model.ubicacion
            
            do{
                try ctxSession.save()
                return true
            }catch{
                return false
            }
    }
    
    func getModels() -> [LocationModel]?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "LocationLDBModel")
        
        do{
            let fetching = try ctxSession.fetch(request)
            var arrayResult:[LocationModel] = []
            for item in fetching {
                let it = item as! LocationLDBModel
                let poliReg = PoligonRegionLocalModel.shared.getModels(idLocation: it.id) ?? []
                arrayResult.append(LocationModel(poligonoRegion: poliReg, _id: it.id, nombreRegion: it.nombreRegion, plazaComercial: it.plazaComercial, ubicacion: it.ubicacion))
            }
            return arrayResult
        }catch{
            return nil
        }
    }
    
    private func ifExist(id:String) -> LocationLDBModel?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "LocationLDBModel")
        request.predicate = NSPredicate(format: "id = %@", id)
        
        do {
            let fetching = try ctxSession.fetch(request).first
            return fetching as? LocationLDBModel
        } catch {
            return nil
        }
    }
    
    func deleteAll(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "LocationLDBModel")
        do{
            let fetching = try ctxSession.fetch(request)
            for object in fetching {
                ctxSession.delete(object as! NSManagedObject)
            }
            try ctxSession.save()
        }catch{
        }
    }
}
