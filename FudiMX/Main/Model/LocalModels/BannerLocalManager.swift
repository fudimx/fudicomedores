//
//  BannerLocalManager.swift
//  FudiMX
//
//  Created by Cristian Canedo on 10/04/22.
//

import Foundation
import CoreData
import UIKit

public class BannerLocalManager{
    private var ctxSession: NSManagedObjectContext = {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let contx = appDelegate.persistentContainer.viewContext
        return contx
    }()
    
    public static let shared: BannerLocalManager = {
            let instance = BannerLocalManager()
            return instance
    }()
    
    private func createModel(model:BannerModel) -> Bool{
        guard let entity = NSEntityDescription.entity(forEntityName: "BannerLDBModel", in: ctxSession) else{return false}
        
        if let entity = ifExist(id: model.id!){
            return updateModel(entity:entity, model: model)
        }else{
            let res = BannerLDBModel(entity: entity, insertInto: ctxSession)
            res.id = model.id
            res.idLocation = model.idLocation
            res.image = model.image
            
            do{
                try ctxSession.save()
                return true
            }catch{
                return false
            }
            
        }
    }
    
    func saveModels(models:[BannerModel], response:@escaping(Bool) -> Void){
        var result = true
        for it in models{
           result = createModel(model: it) && result
        }
        response(result)
    }
    
    func updateModel(entity:BannerLDBModel, model:BannerModel) -> Bool{
        entity.idLocation = model.idLocation
        entity.image = model.image
        do{
            try ctxSession.save()
            return true
        }catch{
            return false
        }
    }
    
    func getModels() -> [BannerModel]?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "BannerLDBModel")
        
        do{
            let fetching = try ctxSession.fetch(request)
            var arrayResult:[BannerModel] = []
            for item in fetching {
                let it = item as! BannerLDBModel
                arrayResult.append(BannerModel(id: it.id, image: it.image, idLocation: it.idLocation))
            }
            return arrayResult
        }catch{
            return nil
        }
    }
    
    private func ifExist(id:String) -> BannerLDBModel?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "BannerLDBModel")
        request.predicate = NSPredicate(format: "id = %@", id)
        
        do {
            let fetching = try ctxSession.fetch(request).first
            return fetching as? BannerLDBModel
        } catch {
            return nil
        }
    }
    func deleteAll(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "BannerLDBModel")
        do{
            let fetching = try ctxSession.fetch(request)
            for object in fetching {
                ctxSession.delete(object as! NSManagedObject)
            }
            try ctxSession.save()
        }catch{
        }
    }
}
