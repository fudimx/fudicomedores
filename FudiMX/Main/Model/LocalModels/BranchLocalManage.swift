//
//  BranchLocalManage.swift
//  FudiMX
//
//  Created by Cristian Canedo on 11/04/22.
//

import Foundation
import CoreData
import UIKit

public class BranchLocalManage{
    private var ctxSession: NSManagedObjectContext = {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let contx = appDelegate.persistentContainer.viewContext
        return contx
    }()
    
    public static let shared: BranchLocalManage = {
            let instance = BranchLocalManage()
            return instance
    }()
    
    private func createModel(model:BranchModel) -> Bool{
        guard let entity = NSEntityDescription.entity(forEntityName: "BranchLDBModel", in: ctxSession) else{return false}
        
        if let entity = ifExist(id: model._id) {
            return updateModel(entity:entity, model: model)
        }else{
            let res = BranchLDBModel(entity: entity, insertInto: ctxSession)
            res.id = model._id
            res.time_zone = model.TIME_ZONE
            res.bActivo = model.bActivo
            res.bSoloRecoger = model.bSoloRecoger
            res.cDireccion = model.cDireccion
            
            res.cNombreRestaurante = model.cNombreRestaurante
            res.cNombreSucursal = model.cNombreSucursal
            res.cTelefono = model.cTelefono
            res.dHoraApertura = model.dHoraApertura
            res.dHoraCierre = model.dHoraCierre
            res.descuentoPicnic = NSNumber(integerLiteral: model.descuentoPicnic)
            res.nCostoEntrega = NSNumber(integerLiteral: model.nCostoEntrega)
            res.nHoraApertura = NSNumber(integerLiteral: model.nHoraApertura)
            res.nHoraCierre = NSNumber(integerLiteral: model.nHoraCierre)
            res.nIdCategoria = NSNumber(integerLiteral:model.nIdCategoria)
            res.nIdFormaPago = NSNumber(integerLiteral:model.nIdFormaPago)
            res.nIdRestaurante = model.nIdRestaurante
            res.nLatitud = NSNumber(value:model.nLatitud)
            res.nLongitud = NSNumber(value:model.nLongitud)
            res.nMinutoApertura = NSNumber(integerLiteral: model.nMinutoApertura)
            res.nMinutoCierre = NSNumber(integerLiteral:model.nMinutoCierre)
            res.nTiempoEntrega = NSNumber(integerLiteral:model.nTiempoEntrega)
            res.nTiempoEspera = NSNumber(integerLiteral:model.nTiempoEspera)
            res.nTipoDescuento = NSNumber(integerLiteral:model.nTipoDescuento)
            res.barraBebidas = model.barraBebidas ?? false
            res.pedidoMinimo = NSNumber(integerLiteral:model.pedidoMinimo ?? 0)
            res.regionEntrega = model.regionEntrega
            res.tipoEntrega = NSNumber(integerLiteral:model.tipoEntrega)
            
            
            
            do{
                try ctxSession.save()
                return true
            }catch{
                return false
            }
            
        }
    }
    
    func saveModels(models:[BranchModel], response:@escaping(Bool) -> Void){
        var result = true
        for it in models{
           result = createModel(model: it) && result
        }
        response(result)
    }
    
    func updateModel(entity: BranchLDBModel, model:BranchModel) -> Bool{
        
        entity.time_zone = model.TIME_ZONE
        entity.bActivo = model.bActivo
        entity.bSoloRecoger = model.bSoloRecoger
        entity.cDireccion = model.cDireccion
        
        entity.cNombreRestaurante = model.cNombreRestaurante
        entity.cNombreSucursal = model.cNombreSucursal
        entity.cTelefono = model.cTelefono
        entity.dHoraApertura = model.dHoraApertura
        entity.dHoraCierre = model.dHoraCierre
        entity.descuentoPicnic = NSNumber(integerLiteral: model.descuentoPicnic)
        entity.nCostoEntrega = NSNumber(integerLiteral: model.nCostoEntrega)
        entity.nHoraApertura = NSNumber(integerLiteral: model.nHoraApertura)
        entity.nHoraCierre = NSNumber(integerLiteral: model.nHoraCierre)
        entity.nIdCategoria = NSNumber(integerLiteral:model.nIdCategoria)
        entity.nIdFormaPago = NSNumber(integerLiteral:model.nIdFormaPago)
        entity.nIdRestaurante = model.nIdRestaurante
        entity.nLatitud = NSNumber(value:model.nLatitud)
        entity.nLongitud = NSNumber(value:model.nLongitud)
        entity.nMinutoApertura = NSNumber(integerLiteral: model.nMinutoApertura)
        entity.nMinutoCierre = NSNumber(integerLiteral:model.nMinutoCierre)
        entity.nTiempoEntrega = NSNumber(integerLiteral:model.nTiempoEntrega)
        entity.nTiempoEspera = NSNumber(integerLiteral:model.nTiempoEspera)
        entity.nTipoDescuento = NSNumber(integerLiteral:model.nTipoDescuento)
        entity.pedidoMinimo = NSNumber(integerLiteral:model.pedidoMinimo ?? 0)
        entity.regionEntrega = model.regionEntrega
        entity.barraBebidas = model.barraBebidas ?? false
        entity.tipoEntrega = NSNumber(integerLiteral:model.tipoEntrega)
            
            do{
                try ctxSession.save()
                return true
            }catch{
                return false
            }
    }
    
    func getModels() -> [BranchModel]?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "BranchLDBModel")
        
        do{
            let fetching = try ctxSession.fetch(request)
            var arrayResult:[BranchModel] = []
            for item in fetching {
                let it = item as! BranchLDBModel
                let branchObject = BranchModel(_id: it.id ?? "", nIdRestaurante: it.nIdRestaurante, cNombreSucursal: it.cNombreSucursal, cDireccion: it.cDireccion, dHoraApertura: it.dHoraApertura, nTiempoEntrega: it.nTiempoEspera as? Int ?? 0, dHoraCierre: it.dHoraCierre, bActivo: it.bActivo, nCostoEntrega: it.nCostoEntrega as? Int ?? 0, nTiempoEspera: it.nTiempoEspera as? Int ?? 0, cTelefono: it.cTelefono, tipoEntrega: it.tipoEntrega as? Int ?? 0, nHoraApertura: it.nHoraApertura as? Int ?? 0, nMinutoApertura: it.nMinutoApertura as? Int ?? 0, nHoraCierre: it.nHoraCierre as? Int ?? 0, nMinutoCierre: it.nMinutoCierre as? Int ?? 0, TIME_ZONE: it.time_zone, bSoloRecoger: it.bSoloRecoger, nLatitud: it.nLatitud as? Double ?? 0.0, nLongitud: it.nLongitud as? Double ?? 0.0, regionEntrega: it.regionEntrega, cNombreRestaurante: it.cNombreRestaurante, nIdCategoria: it.nIdCategoria as? Int ?? 0, nIdFormaPago: it.nIdFormaPago as? Int ?? 0, pedidoMinimo: it.pedidoMinimo as? Int ?? 0, nTipoDescuento: it.nTipoDescuento as? Int ?? 0, descuentoPicnic: it.descuentoPicnic as? Int ?? 0, barraBebidas: it.barraBebidas , diasDescuentoPicnic: [], lstRangoEntrega: [])
                arrayResult.append(branchObject)
            }
            return arrayResult
        }catch{
            return nil
        }
    }
    func getModelsById(idLocation:String) -> [BranchModel]?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "BranchLDBModel")
        request.predicate = NSPredicate(format: "regionEntrega = %@", idLocation)
        do{
            let fetching = try ctxSession.fetch(request)
            var arrayResult:[BranchModel] = []
            for item in fetching {
                let it = item as! BranchLDBModel
                let branchObject = BranchModel(_id: it.id ?? "", nIdRestaurante: it.nIdRestaurante, cNombreSucursal: it.cNombreSucursal, cDireccion: it.cDireccion, dHoraApertura: it.dHoraApertura, nTiempoEntrega: it.nTiempoEspera as? Int ?? 0, dHoraCierre: it.dHoraCierre, bActivo: it.bActivo, nCostoEntrega: it.nCostoEntrega as? Int ?? 0, nTiempoEspera: it.nTiempoEspera as? Int ?? 0, cTelefono: it.cTelefono, tipoEntrega: it.tipoEntrega as? Int ?? 0, nHoraApertura: it.nHoraApertura as? Int ?? 0, nMinutoApertura: it.nMinutoApertura as? Int ?? 0, nHoraCierre: it.nHoraCierre as? Int ?? 0, nMinutoCierre: it.nMinutoCierre as? Int ?? 0, TIME_ZONE: it.time_zone, bSoloRecoger: it.bSoloRecoger, nLatitud: it.nLatitud as? Double ?? 0.0, nLongitud: it.nLongitud as? Double ?? 0.0, regionEntrega: it.regionEntrega, cNombreRestaurante: it.cNombreRestaurante, nIdCategoria: it.nIdCategoria as? Int ?? 0, nIdFormaPago: it.nIdFormaPago as? Int ?? 0, pedidoMinimo: it.pedidoMinimo as? Int ?? 0, nTipoDescuento: it.nTipoDescuento as? Int ?? 0, descuentoPicnic: it.descuentoPicnic as? Int ?? 0,  barraBebidas: it.barraBebidas, diasDescuentoPicnic: [], lstRangoEntrega: [])
                arrayResult.append(branchObject)
            }
            return arrayResult
        }catch{
            return nil
        }
    }
    private func ifExist(id:String) -> BranchLDBModel?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "BranchLDBModel")
        request.predicate = NSPredicate(format: "id = %@", id)
        
        do {
            let fetching = try ctxSession.fetch(request).first
            return fetching as? BranchLDBModel
        } catch {
            return nil
        }
    }
    func deleteAll(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "BranchLDBModel")
        do{
            let fetching = try ctxSession.fetch(request)
            for object in fetching {
                ctxSession.delete(object as! NSManagedObject)
            }
            try ctxSession.save()
        }catch{
        }
    }
}
