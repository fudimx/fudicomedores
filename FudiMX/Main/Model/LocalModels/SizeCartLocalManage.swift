//
//  SizeCartLDBModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 08/05/22.
//

import Foundation
import CoreData
import UIKit

class SizeCartLocalManage{
    private var ctxSession: NSManagedObjectContext = {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let contx = appDelegate.persistentContainer.viewContext
        return contx
    }()
    
    public static let shared: SizeCartLocalManage = {
            let instance = SizeCartLocalManage()
            return instance
    }()
    
    private func createModel(model:SizeCartModel, idShoppingCart:String) -> Bool{
        guard let entity = NSEntityDescription.entity(forEntityName: "SizeCartLDBModel", in: ctxSession) else{return false}
        
        if let entity = ifExist(model: model){
            return updateModel(entity:entity, model: model)
        }else{
            let res = SizeCartLDBModel(entity: entity, insertInto: ctxSession)
            res.quantity = NSNumber(integerLiteral: model.quantity ?? 0)
            res.bSeleccionado = model.bSeleccionado
            res.cNombreTamanio = model.cNombreTamanio
            res.nPrecio = NSNumber(value: model.nPrecio ?? 0.0)
            res.idShoppingCart = idShoppingCart
            
            do{
                try ctxSession.save()
                return true
            }catch{
                return false
            }
            
        }
    }
    
    func saveModels(models:[SizeCartModel], idShoppingCart:String){
        var result = true
        for it in models{
            result = createModel(model: it, idShoppingCart:idShoppingCart) && result
        }
    }
    
    func updateModel(entity:SizeCartLDBModel, model:SizeCartModel) -> Bool{
        entity.quantity = NSNumber(integerLiteral: model.quantity ?? 0)
        entity.bSeleccionado = model.bSeleccionado
        entity.cNombreTamanio = model.cNombreTamanio
        entity.nPrecio = NSNumber(value: model.nPrecio ?? 0.0)
        
        do{
            try ctxSession.save()
            return true
        }catch{
            return false
        }
    }
    
    func getModels(id:String) -> [SizeCartModel]?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "SizeCartLDBModel")
        request.predicate = NSPredicate(format: "idShoppingCart = %@", id)
        do{
            let fetching = try ctxSession.fetch(request)
            var arrayResult:[SizeCartModel] = []
            for item in fetching {
                let it = item as! SizeCartLDBModel
                arrayResult.append(SizeCartModel(bSeleccionado: it.bSeleccionado, cNombreTamanio: it.cNombreTamanio ?? "", nPrecio: it.nPrecio as? Double ?? 0.0, quantity: it.quantity as? Int ?? 0, idShoppingCart: it.idShoppingCart))
            }
            return arrayResult
        }catch{
            return nil
        }
    }
    func clearModel(idShopping:String){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "SizeCartLDBModel")
        request.predicate = NSPredicate(format: "idShoppingCart = %@", idShopping)
        do{
            let fetching = try ctxSession.fetch(request)
            for item in fetching {
                let it = item as! SizeCartLDBModel
                ctxSession.delete(it)
                try ctxSession.save()
            }
        }catch{
        }
    }
    private func ifExist(model:SizeCartModel) -> SizeCartLDBModel?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "SizeCartLDBModel")
        let nameSizepredicate = NSPredicate(format: "cNombreTamanio = %@", model.cNombreTamanio ?? "")
        let pricePredicate = NSPredicate(format: "nPrecio = %@", NSNumber(value: model.nPrecio ?? 0.0) )
        let idCartPredicate = NSPredicate(format: "idShoppingCart = %@", model.idShoppingCart ?? "")
        
        request.predicate = NSCompoundPredicate(
            andPredicateWithSubpredicates: [nameSizepredicate,pricePredicate,idCartPredicate]
        )
        
        do {
            let fetching = try ctxSession.fetch(request).first
            return fetching as? SizeCartLDBModel
        } catch {
            return nil
        }
    }
    func deleteAll(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "SizeCartLDBModel")
        do{
            let fetching = try ctxSession.fetch(request)
            for object in fetching {
                ctxSession.delete(object as! NSManagedObject)
            }
            try ctxSession.save()
        }catch{
        }
    }
}
