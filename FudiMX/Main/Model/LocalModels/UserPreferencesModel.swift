//
//  UserPreferencesModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 21/04/22.
//

import Foundation

public class UserPreferencesModel{
    var userDefaults = UserDefaults.standard
    
    public static let shared: UserPreferencesModel = {
            let instance = UserPreferencesModel()
            return instance
    }()
    
    func initUserInfo(data:UserPedidoModel?){
        guard let userData = data else{
            userDefaults.removeObject(forKey: "_userInfo")
            return
        }
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(userData) {
            userDefaults.set(encoded, forKey: "_userInfo")
        }
    }
    
    func getUserinfoModel()-> UserPedidoModel?{
        guard let userModel = userDefaults.object(forKey: "_userInfo") else{return nil}
        let decoder = JSONDecoder()
        
        guard let dataDecode = try? decoder.decode(UserPedidoModel.self, from: userModel as? Data ?? Data()) else{
            return nil
        }
        return dataDecode
    }
    func iniBranchCartModel(data:BranchModel?){
        guard let branchData = data else{
            userDefaults.removeObject(forKey: "_branchCartSelected")
            return
        }
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(branchData) {
            userDefaults.set(encoded, forKey: "_branchCartSelected")
        }
    }
    func getBranchCartModel()->BranchModel?{
        guard let dataBranch = userDefaults.object(forKey: "_branchCartSelected") else{return nil}
        let decoder = JSONDecoder()
        
        guard let dataDecode = try? decoder.decode(BranchModel.self, from: dataBranch as? Data ?? Data()) else{
            return nil
        }
        return dataDecode
    }
    func iniBranchModel(data:BranchModel?){
        guard let branchData = data else{
            userDefaults.removeObject(forKey: "_branchSelected")
            return
        }
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(branchData) {
            userDefaults.set(encoded, forKey: "_branchSelected")
        }
    }
     
    func getBranchModel()->BranchModel?{
        guard let dataBranch = userDefaults.object(forKey: "_branchSelected") else{return nil}
        let decoder = JSONDecoder()
        
        guard let dataDecode = try? decoder.decode(BranchModel.self, from: dataBranch as? Data ?? Data()) else{
            return nil
        }
        return dataDecode
    }
    
    func initMethodPay(data:String){
        userDefaults.set(data, forKey: "_methodPaySelected")
    }
    func getMethodPay() -> String?{
        guard let result = userDefaults.string(forKey: "_methodPaySelected") else{return nil}
        return result
    }
    
    func initLocationModel(data:LocationModel){
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(data) {
            userDefaults.set(encoded, forKey: "_locationSelected")
        }
    }
    func getLocationModel() -> LocationModel?{
        guard let dataLocation = userDefaults.object(forKey: "_locationSelected") else{return nil}
        let decoder = JSONDecoder()
        
        guard let dataDecode = try? decoder.decode(LocationModel.self, from: dataLocation as! Data) else{
            return nil
        }
        return dataDecode
    }
    func initLastMenuDate(date:Date){
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(date) {
            userDefaults.set(encoded, forKey: "_lastDateMenu")
        }
    }
    func getLastMenuDate() -> Date?{
        guard let dataLocation = userDefaults.object(forKey: "_lastDateMenu") else{return nil}
        let decoder = JSONDecoder()
        guard let dataDecode = try? decoder.decode(Date.self, from: dataLocation as! Data) else{
            return nil
        }
        return dataDecode
    }
    
}
