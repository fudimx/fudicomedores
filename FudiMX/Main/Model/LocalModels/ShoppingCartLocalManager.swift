//
//  ShoppingCartLocalManager.swift
//  FudiMX
//
//  Created by Cristian Canedo on 19/04/22.
//

import Foundation
import CoreData
import UIKit

public class ShoppingCartLocalManager{
    private var ctxSession: NSManagedObjectContext = {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let contx = appDelegate.persistentContainer.viewContext
        return contx
    }()
    
    public static let shared: ShoppingCartLocalManager = {
            let instance = ShoppingCartLocalManager()
            return instance
    }()
    
    private func createModel(model:ShoppingCartModel) -> Bool{
        guard let entity = NSEntityDescription.entity(forEntityName: "ShoppingCartLDBModel", in: ctxSession) else{return false}
        
        if let entity = ifExist(idProduct: model.idProduct!) {
            return updateModel(entity:entity, model: model)
        }else{
            
            let res = ShoppingCartLDBModel(entity: entity, insertInto: ctxSession)
            res.id = model.id
            res.idProduct = model.idProduct
            res.total = NSNumber(value: model.total ?? 0.0)
            res.quantity = NSNumber(integerLiteral: model.quantity ?? 0)
            res.notes = model.notes
            res.status = model.status ?? false
            SizeCartLocalManage.shared.saveModels(models: model.sizes, idShoppingCart: model.id ?? "")
            
            do{
                try ctxSession.save()
                return true
            }catch{
                return false
            }
        }
    }
    
    
    
    
    func saveModels(model:ShoppingCartModel, response:@escaping(Bool) -> Void){
        var result = true
        result = createModel(model: model) && result
        response(result)
    }
    
    func updateModel(entity: ShoppingCartLDBModel, model:ShoppingCartModel) -> Bool{
        let intOne = entity.quantity as? Int ?? 0
        let intTwo = model.quantity ?? 0
        let sum = intOne + intTwo
        
        let entityTotal = entity.total as? Double ?? 0.0
        let modelTotal = model.total ?? 0.0
        
        entity.idProduct = model.idProduct
        entity.quantity = NSNumber(integerLiteral: sum)
        entity.notes = model.notes
        entity.total = NSNumber(value: entityTotal + modelTotal)
        entity.status = model.status ?? false
        SizeCartLocalManage.shared.saveModels(models: model.sizes, idShoppingCart: model.id ?? "")
        do{
            try ctxSession.save()
            return true
        }catch{
            return false
        }
    }
    
    func getModels() -> [ShoppingCartModel]?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ShoppingCartLDBModel")
        do{
            let fetching = try ctxSession.fetch(request)
            var arrayResult:[ShoppingCartModel] = []
            for item in fetching {
                let it = item as! ShoppingCartLDBModel
                var name = ""
                if let nameProduct = ProductsLocalmanager.shared.getItemById(id: it.idProduct ?? ""){
                    name = nameProduct.cNombrePlatillo
                }
                if it.status {
                    let sizes = SizeCartLocalManage.shared.getModels(id: it.id ?? "")
                    arrayResult.append(ShoppingCartModel.init(id: it.id ?? "", idProduct: it.idProduct ?? "", notes: it.notes ?? "", quantity: it.quantity as? Int ?? 0, total: it.total as? Double ?? 0.0, nameProduct: name, status: it.status, addOns: [], sizes:sizes ?? []))
                }
            }
            return arrayResult
        }catch{
            return nil
        }
    }
    
    func deleteModel() -> Bool{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ShoppingCartLDBModel")
        do{
            let results:NSArray = try ctxSession.fetch(request) as NSArray
            for object in results {
                let it = object as! ShoppingCartLDBModel
                ctxSession.delete(it)
            }
            try ctxSession.save()
            UserPreferencesModel.shared.iniBranchCartModel(data: nil)
            return true
        }catch{
            return false
        }
    }
    func clearModel(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ShoppingCartLDBModel")
        do{
            let fetching = try ctxSession.fetch(request)
            for item in fetching {
                let it = item as! ShoppingCartLDBModel
                it.status = false
                try ctxSession.save()
            }
        }catch{
        }
    }
    func deleteModelById(id:String) -> Bool{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ShoppingCartLDBModel")
        request.predicate = NSPredicate(format: "id = %@", id)
        do{
            let results = try ctxSession.fetch(request).first
            ctxSession.delete(results as! NSManagedObject)
            try ctxSession.save()
            return true
        }catch{
            return false
        }
    }
    func getCart() -> WatchCartItems?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ShoppingCartLDBModel")
        do{
            let fetching = try ctxSession.fetch(request)
            if !fetching.isEmpty{
                var total = 0.0
                var count = 0
                for it in fetching{
                    let item = it as! ShoppingCartLDBModel
                    if item.status {
                    total += item.total as? Double ?? 0.0
                    count += 1
                    }
                }
                if(count > 0){
                    return WatchCartItems(totalPay: total, totalItems: count)
                }else{
                    return nil
                }
            }else{
                return nil
            }
        }catch{
            return nil
        }
    }
    private func ifExist(idProduct:String) -> ShoppingCartLDBModel?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ShoppingCartLDBModel")
        request.predicate = NSPredicate(format: "idProduct = %@", idProduct)
        
        do {
            let fetching = try ctxSession.fetch(request).first
            return fetching as? ShoppingCartLDBModel
        } catch {
            return nil
        }
    }
    func deleteAll(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ShoppingCartLDBModel")
        do{
            let fetching = try ctxSession.fetch(request)
            for object in fetching {
                ctxSession.delete(object as! NSManagedObject)
            }
            try ctxSession.save()
        }catch{
        }
    }
}
