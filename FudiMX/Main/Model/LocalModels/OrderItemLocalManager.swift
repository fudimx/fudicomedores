//
//  OrderItemLocalManager.swift
//  FudiMX
//
//  Created by Cristian Canedo on 21/04/22.
//

import Foundation
import CoreData
import UIKit

public class OrderItemLocalManager{
    
    private var ctxSession: NSManagedObjectContext = {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let contx = appDelegate.persistentContainer.viewContext
        return contx
    }()
    
    public static let shared: OrderItemLocalManager = {
            let instance = OrderItemLocalManager()
            return instance
    }()
    
    private func createModel(model:OrderItemModel, idOrder:String) -> Bool{
        guard let entity = NSEntityDescription.entity(forEntityName: "OrderItemLDBModel", in: ctxSession) else{return false}
        
        if model.id != nil, let entity = ifExist(id: model.id ?? ""){
            return updateModel(entity:entity, model: model)
        }else{
            let res = OrderItemLDBModel(entity: entity, insertInto: ctxSession)
            res.id = UUID().uuidString
            res.idProduct = model.idProduct
            res.idOrder = idOrder
            res.idShoppingCart = model.idShoppingCart
            res.quantity = NSNumber(integerLiteral: model.quantity ?? 0)
            res.total = NSNumber(value: model.total ?? 0.0)
            res.notes = model.notes ?? ""
        
            do{
                try ctxSession.save()
                return true
            }catch{
                return false
            }
            
        }
    }
    
    func saveModels(idOrder:String, models:[OrderItemModel], completion:@escaping(Bool) -> Void){
        var result = true
        for it in models{
            result = createModel(model: it, idOrder: idOrder) && result
        }
        completion(result)
    }
    
    func updateModel(entity:OrderItemLDBModel, model:OrderItemModel) -> Bool{
        entity.idOrder = model.idOrder ?? ""
        entity.idProduct = model.idProduct
        entity.idShoppingCart = model.idShoppingCart
        entity.quantity = NSNumber(integerLiteral: model.quantity ?? 0)
        entity.total = NSNumber(value: model.total ?? 0.0)
        entity.notes = model.notes ?? ""
        
        do{
            try ctxSession.save()
            return true
        }catch{
            return false
        }
    }
    
    func getModels(idOrder:String) -> [OrderItemModel]?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "OrderItemLDBModel")
        request.predicate = NSPredicate(format: "idOrder = %@", idOrder)
        do{
            let fetching = try ctxSession.fetch(request)
            var arrayResult:[OrderItemModel] = []
            for item in fetching {
                let it = item as! OrderItemLDBModel
                let sizes = SizeCartLocalManage.shared.getModels(id: it.idShoppingCart)
                arrayResult.append(OrderItemModel(id: it.id , idOrder: it.idOrder, idProduct: it.idProduct, idShoppingCart: it.idShoppingCart, quantity: it.quantity as? Int ?? 0 ,total: it.total as? Double ?? 0.0 , addOns: [], sizes: sizes ?? []))
            }
            return arrayResult
        }catch{
            return nil
        }
    }
    
    func getModelByID(id:String) -> OrderItemModel?{
        guard let result = ifExist(id: id) else{ return nil}
        let sizes = SizeCartLocalManage.shared.getModels(id:result.idShoppingCart)
        let response =  OrderItemModel(id: result.id, idOrder: result.idOrder, idProduct: result.idProduct, idShoppingCart: result.idShoppingCart, addOns: [], sizes: sizes ?? [])
        return response
    }
    
    func deleteModel(id:String)-> Bool{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "OrderItemLDBModel")
        request.predicate = NSPredicate(format: "id = %@", id)
        do{
            let results = try ctxSession.fetch(request).first
            let item = results as? OrderItemLDBModel
            ctxSession.delete(results as! NSManagedObject)
            try ctxSession.save()
            SizeCartLocalManage.shared.clearModel(idShopping: item?.idShoppingCart ?? "")
            return true
        }catch{
            return false
        }
    }
    private func ifExist(id:String) -> OrderItemLDBModel?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "OrderItemLDBModel")
        request.predicate = NSPredicate(format: "id = %@", id)
        
        do {
            let fetching = try ctxSession.fetch(request).first
            return fetching as? OrderItemLDBModel
        } catch {
            return nil
        }
    }
    func deleteAll(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "OrderItemLDBModel")
        do{
            let fetching = try ctxSession.fetch(request)
            for object in fetching {
                ctxSession.delete(object as! NSManagedObject)
            }
            try ctxSession.save()
        }catch{
        }
    }
}
