//
//  MenuLocalManager.swift
//  FudiMX
//
//  Created by Cristian Canedo on 16/04/22.
//

import Foundation
import CoreData
import UIKit


public class MenuLocalManager{
    
    private var ctxSession: NSManagedObjectContext = {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let contx = appDelegate.persistentContainer.viewContext
        return contx
    }()
    
    public static let shared: MenuLocalManager = {
            let instance = MenuLocalManager()
            return instance
    }()
    
    private func createModel(model:MenuModel) -> Bool{
        guard let entity = NSEntityDescription.entity(forEntityName: "MenuLDBModel", in: ctxSession) else{return false}
        
        if let modelExist = ifExist(id: model._id)  {
            return updateModel(entity: modelExist, model: model)
        }else{
            var response = true
            let res = MenuLDBModel(entity: entity, insertInto: ctxSession)
            res.id = model._id
            res.cDescripcion = model.cDescripcion
            res.bActivo = model.bActivo
            res.nIdRestaurante = model.nIdRestaurante

            ProductsLocalmanager.shared.saveModels(models: model.lstPlatillos){ respuesta in
                if !respuesta{
                    response = false
                }
            }
            
            if response{
                do{
                    try ctxSession.save()
                    return true
                }catch{
                    return false
                }
            }else{
                return false
            }
        }
    }
    
    func saveModels(models:[MenuModel], response:@escaping(Bool) -> Void){
        var result = true
        for it in models{
           result = createModel(model: it) && result
        }
        response(result)
    }
    
    func updateModel(entity:MenuLDBModel, model:MenuModel) -> Bool{
        var response = true
        entity.id = model._id
        entity.cDescripcion = model.cDescripcion
        entity.bActivo = model.bActivo
        entity.nIdRestaurante = model.nIdRestaurante
        
        ProductsLocalmanager.shared.saveModels(models: model.lstPlatillos){ respuesta in
            if !respuesta{
                response = false
            }
        }
        if response {
            do{
                try ctxSession.save()
                return true
            }catch{
                return false
            }
        }else{
            return false
        }
    }
    
    func getModels(idBranch:String) -> [MenuModel]?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MenuLDBModel")
        request.predicate = NSPredicate(format: "nIdRestaurante = %@", idBranch)
        do{
            let fetching = try ctxSession.fetch(request)
            var arrayResult:[MenuModel] = []
            var index = 0
            for item in fetching {
                let it = item as! MenuLDBModel
                var models:[ProductsModel] = []
                
                if let arrModel = ProductsLocalmanager.shared.getModelsById(idMenu: it.id ?? "") {
                    models = arrModel
                }
                let isSelected = index == 0
                arrayResult.append(MenuModel.init(it.id ?? "" , bActivo: it.bActivo, isSelected, models, description: it.cDescripcion ?? "", it.nIdRestaurante ?? ""))
                index += 1
            }
            return arrayResult
        }catch{
            return nil
        }
    }
    
    private func ifExist(id:String) -> MenuLDBModel?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MenuLDBModel")
        request.predicate = NSPredicate(format: "id = %@", id)
        
        do {
            let fetching = try ctxSession.fetch(request).first
            return fetching as? MenuLDBModel
        } catch {
            return nil
        }
    }
    func deleteAll(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MenuLDBModel")
        do{
            let fetching = try ctxSession.fetch(request)
            for object in fetching {
                ctxSession.delete(object as! NSManagedObject)
            }
            try ctxSession.save()
        }catch{
        }
    }
}
