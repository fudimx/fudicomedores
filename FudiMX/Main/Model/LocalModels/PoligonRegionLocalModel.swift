//
//  PoligonRegionLDBModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 22/04/22.
//

import Foundation
import CoreData
import UIKit

class PoligonRegionLocalModel{
    private var ctxSession: NSManagedObjectContext = {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let contx = appDelegate.persistentContainer.viewContext
        return contx
    }()
    
    public static let shared: PoligonRegionLocalModel = {
            let instance = PoligonRegionLocalModel()
            return instance
    }()
    
    private func createModel(model:PoligonoRegion, idLocation:String) -> Bool{
        guard let entity = NSEntityDescription.entity(forEntityName: "PoligonLDBModel", in: ctxSession) else{return false}
        
        if let entity = ifExist(model: model, idLocation: idLocation){
            return updateModel(entity:entity, model: model, idLocation:idLocation)
        }else{
            let res = PoligonLDBModel(entity: entity, insertInto: ctxSession)
            res.idLocation = idLocation
            res.lat = NSNumber(value: model.lat)
            res.lng = NSNumber(value: model.lng)
            
            do{
                try ctxSession.save()
                return true
            }catch{
                return false
            }
            
        }
    }
    
    func saveModels(models:[PoligonoRegion], idLocation:String){
        var result = true
        for it in models{
            result = createModel(model: it, idLocation: idLocation) && result
        }
    }
    
    func updateModel(entity:PoligonLDBModel, model:PoligonoRegion, idLocation:String) -> Bool{
        entity.lat = NSNumber(value:model.lat)
        entity.lng = NSNumber(value:model.lng)
        entity.idLocation = idLocation
        do{
            try ctxSession.save()
            return true
        }catch{
            return false
        }
    }
    
    func getModels(idLocation:String) -> [PoligonoRegion]?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "PoligonLDBModel")
        request.predicate = NSPredicate(format: "idLocation = %@", idLocation)
        
        do{
            let fetching = try ctxSession.fetch(request)
            var arrayResult:[PoligonoRegion] = []
            for item in fetching {
                let it = item as! PoligonLDBModel
                arrayResult.append(PoligonoRegion( lat: it.lat as? Double ?? 0.0, lng: it.lng as? Double ?? 0.0))
            }
            return arrayResult
        }catch{
            return nil
        }
    }
    
    private func ifExist(model:PoligonoRegion, idLocation:String) -> PoligonLDBModel?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "PoligonLDBModel")
        let idPredicate = NSPredicate(format: "idLocation = %@", idLocation)
        let latPredicate = NSPredicate(format: "lat = %@", NSNumber(value:model.lat))
        let lngPredicate = NSPredicate(format: "lng = %@", NSNumber(value:model.lng))
        request.predicate = NSCompoundPredicate(
            andPredicateWithSubpredicates: [idPredicate,latPredicate,lngPredicate]
        )
        do {
            let fetching = try ctxSession.fetch(request).first
            return fetching as? PoligonLDBModel
        } catch {
            return nil
        }
    }
    func deleteAll(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "PoligonLDBModel")
        do{
            let fetching = try ctxSession.fetch(request)
            for object in fetching {
                ctxSession.delete(object as! NSManagedObject)
            }
            try ctxSession.save()
        }catch{
        }
    }
}
