//
//  AddOnsLocalManage.swift
//  FudiMX
//
//  Created by Cristian Canedo on 26/04/22.
//

import Foundation
import CoreData
import UIKit

class AddOnsLocalManage{
    private var ctxSession: NSManagedObjectContext = {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let contx = appDelegate.persistentContainer.viewContext
        return contx
    }()
    
    public static let shared: AddOnsLocalManage = {
            let instance = AddOnsLocalManage()
            return instance
    }()
    
    private func createModel(model:AddOnsModel, idProduct:String) -> Bool{
        guard let entity = NSEntityDescription.entity(forEntityName: "AddOnsLDBModel", in: ctxSession) else{return false}
        
        if let entity = ifExist(model: model, idProduct: idProduct){
            return updateModel(entity:entity, model: model, idProduct:idProduct)
        }else{
            let res = AddOnsLDBModel(entity: entity, insertInto: ctxSession)
            res.complementos = UtilClass.stringArrayToData(stringArray: model.complementos) ?? Data()
            res.pasoPreparacion = model.pasoPreparacion
            res.idProduct = idProduct
            
            do{
                try ctxSession.save()
                return true
            }catch{
                return false
            }
            
        }
    }
    
    func saveModels(models:[AddOnsModel], idProduct:String){
        var result = true
        for it in models{
            result = createModel(model: it, idProduct: idProduct) && result
        }
    }
    
    func updateModel(entity:AddOnsLDBModel, model:AddOnsModel, idProduct:String) -> Bool{
        entity.complementos = UtilClass.stringArrayToData(stringArray: model.complementos) ?? Data()
        entity.pasoPreparacion = model.pasoPreparacion
        entity.idProduct = idProduct
        do{
            try ctxSession.save()
            return true
        }catch{
            return false
        }
    }
    
    func getModels(idProduct:String) -> [AddOnsModel]?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "AddOnsLDBModel")
        request.predicate = NSPredicate(format: "idProduct = %@", idProduct)
        
        do{
            let fetching = try ctxSession.fetch(request)
            var arrayResult:[AddOnsModel] = []
            for item in fetching {
                let it = item as! AddOnsLDBModel
                var isSelectedArray:[Bool] = []
                guard let array = UtilClass.dataToStringArray(data: it.complementos) else { return [] }
                array.forEach{it in
                    isSelectedArray.append(false)
                }
                arrayResult.append(AddOnsModel(complementos: array, pasoPreparacion: it.pasoPreparacion, complementosSelect: isSelectedArray))
            }
            return arrayResult
        }catch{
            return nil
        }
    }
    
    private func ifExist(model:AddOnsModel, idProduct:String) -> AddOnsLDBModel?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "AddOnsLDBModel")
        let idPredicate = NSPredicate(format: "idProduct = %@", idProduct)
        request.predicate = idPredicate
        
        do {
            let fetching = try ctxSession.fetch(request).first
            return fetching as? AddOnsLDBModel
        } catch {
            return nil
        }
    }
    
    func deleteAll(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "AddOnsLDBModel")
        do{
            let fetching = try ctxSession.fetch(request)
            for object in fetching {
                ctxSession.delete(object as! NSManagedObject)
            }
            try ctxSession.save()
        }catch{
        }
    }
}
