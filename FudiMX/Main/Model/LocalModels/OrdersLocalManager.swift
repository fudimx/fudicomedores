//
//  OrdersLocalManager.swift
//  FudiMX
//
//  Created by Cristian Canedo on 21/04/22.
//

import Foundation
import CoreData
import UIKit

public class OrdersLocalManager{
    
    private var ctxSession: NSManagedObjectContext = {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let contx = appDelegate.persistentContainer.viewContext
        return contx
    }()
    
    public static let shared: OrdersLocalManager = {
            let instance = OrdersLocalManager()
            return instance
    }()
    
    private func createModel(model:OrderModel) -> String?{
        guard let entity = NSEntityDescription.entity(forEntityName: "OrderLDBModel", in: ctxSession) else{return nil}
        
        if model.id != nil, let entity = ifExist(id: model.id!){
            return updateModel(entity:entity, model: model)
        }else{
            let res = OrderLDBModel(entity: entity, insertInto: ctxSession)
            res.id = UUID().uuidString
            res.total = NSNumber(value: model.total ?? 0.0)
            res.date = model.date
            res.idBranch = model.idBranch
            res.idLocation = model.idLocation
            res.idMethodPay = model.idMethodPay
            res.status = model.status
            
            do{
                try ctxSession.save()
                return res.id
            }catch{
                return nil
            }
            
        }
    }
    
    func saveModels(model:OrderModel, completion:@escaping(_ isCreated:Bool,_ model:String) -> Void){
        guard let result = createModel(model: model) else {
            completion(false, "")
            return
        }
        OrderItemLocalManager.shared.saveModels(idOrder:result, models: model.items){ response in
            completion(response, result)
        }
    }
    
    func updateModel(entity:OrderLDBModel, model:OrderModel) -> String?{
        entity.idBranch = model.idBranch
        entity.idLocation = model.idLocation
        entity.idMethodPay = model.idMethodPay
        entity.total = NSNumber(value: model.total ?? 0.0)
        entity.status = model.status
        
        do{
            try ctxSession.save()
            return entity.id
        }catch{
            return nil
        }
    }
    
    func getModels() -> [OrderModel]?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "OrderLDBModel")
        
        do{
            let fetching = try ctxSession.fetch(request)
            var arrayResult:[OrderModel] = []
            for item in fetching {
                let it = item as! OrderLDBModel
                arrayResult.append(OrderModel(id: it.id ?? "", date: it.date ?? Date(), idBranch: it.idBranch ?? "", idLocation: it.idLocation ?? "", idMethodPay: it.idMethodPay ?? "", status: it.status ?? "", total: it.total as? Double ?? 0))
            }
            return arrayResult
        }catch{
            return nil
        }
    }
    
    func getModelByID(id:String) -> OrderModel?{
        guard let result = ifExist(id: id) else{ return nil}
        
        let itemsModel = OrderItemLocalManager.shared.getModels(idOrder: id)
        let response =  OrderModel(id: result.id ?? "", date: result.date ?? Date(), idBranch: result.idBranch ?? "", idLocation: result.idLocation ?? "", idMethodPay: result.idMethodPay ?? "", status: result.status ?? "", total: result.total as? Double ?? 0.0, items: itemsModel ?? [])
        return response
    }
    
    private func ifExist(id:String) -> OrderLDBModel?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "OrderLDBModel")
        request.predicate = NSPredicate(format: "id = %@", id)
        
        do {
            let fetching = try ctxSession.fetch(request).first
            return fetching as? OrderLDBModel
        } catch {
            return nil
        }
    }
    func deleteAll(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "OrderLDBModel")
        do{
            let fetching = try ctxSession.fetch(request)
            for object in fetching {
                ctxSession.delete(object as! NSManagedObject)
            }
            try ctxSession.save()
        }catch{
        }
    }
}
