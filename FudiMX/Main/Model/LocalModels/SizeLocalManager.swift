//
//  SizeLocalManager.swift
//  FudiMX
//
//  Created by Cristian Canedo on 26/04/22.
//

import Foundation
import CoreData
import UIKit

class SizeLocalManager{
    private var ctxSession: NSManagedObjectContext = {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let contx = appDelegate.persistentContainer.viewContext
        return contx
    }()
    
    public static let shared: SizeLocalManager = {
            let instance = SizeLocalManager()
            return instance
    }()
    
    private func createModel(model:SizeModel, idProduct:String) -> Bool{
        guard let entity = NSEntityDescription.entity(forEntityName: "ProductsSizeLDBModel", in: ctxSession) else{return false}
        
        if let entity = ifExist(model: model, idProduct: idProduct){
            return updateModel(entity:entity, model: model, idProduct:idProduct)
        }else{
            let res = ProductsSizeLDBModel(entity: entity, insertInto: ctxSession)
            res.cDescripcion = model.cDescripcion ?? ""
            res.cNombreTamanio = model.cNombreTamanio
            res.nPrecio = model.nPrecio
            res.idProduct = idProduct
            
            do{
                try ctxSession.save()
                return true
            }catch{
                return false
            }
            
        }
    }
    
    func saveModels(models:[SizeModel], idProduct:String){
        var result = true
        for it in models{
            result = createModel(model: it, idProduct: idProduct) && result
        }
    }
    
    func updateModel(entity:ProductsSizeLDBModel, model:SizeModel, idProduct:String) -> Bool{
        entity.cDescripcion = model.cDescripcion ?? ""
        entity.cNombreTamanio = model.cNombreTamanio
        entity.nPrecio = model.nPrecio
        entity.idProduct = idProduct
        do{
            try ctxSession.save()
            return true
        }catch{
            return false
        }
    }
    
    func getModels(idProduct:String) -> [SizeModel]?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ProductsSizeLDBModel")
        request.predicate = NSPredicate(format: "idProduct = %@", idProduct)
        
        do{
            let fetching = try ctxSession.fetch(request)
            var arrayResult:[SizeModel] = []
            for item in fetching {
                let it = item as! ProductsSizeLDBModel
                arrayResult.append(SizeModel(cNombreTamanio: it.cNombreTamanio , cDescripcion: it.cDescripcion, nPrecio: it.nPrecio))
            }
            return arrayResult
        }catch{
            return nil
        }
    }
    
    private func ifExist(model:SizeModel, idProduct:String) -> ProductsSizeLDBModel?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ProductsSizeLDBModel")
        let idPredicate = NSPredicate(format: "idProduct = %@", idProduct)
        let namePredicate = NSPredicate(format: "cNombreTamanio = %@", model.cNombreTamanio)
        request.predicate = NSCompoundPredicate(
            andPredicateWithSubpredicates: [idPredicate,namePredicate]
        )
        do {
            let fetching = try ctxSession.fetch(request).first
            return fetching as? ProductsSizeLDBModel
        } catch {
            return nil
        }
    }
    func deleteAll(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ProductsSizeLDBModel")
        do{
            let fetching = try ctxSession.fetch(request)
            for object in fetching {
                ctxSession.delete(object as! NSManagedObject)
            }
            try ctxSession.save()
        }catch{
        }
    }
}
