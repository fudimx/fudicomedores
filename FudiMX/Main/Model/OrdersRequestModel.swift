//
//  OrdersRequestModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 08/05/22.
//

import Foundation
import Alamofire

private let timeoutInterval: Double = 10.0
private var configuration = Configuration()

private var dictionary:NSDictionary? {
    guard let path = Bundle.main.path(forResource: "Constants", ofType: "plist"), let dictionary = NSDictionary(contentsOfFile: path) else {
        return nil
    }
    return dictionary
}

func getFolioOrderRequest(data:String, completion:@escaping(_ folio:String?,_ error:ErrorResponse?) -> Void){
    
    guard let pncgrpDic = dictionary else {return}
    let baseURL = configuration.environment.baseURL
    let pathDictionary = pncgrpDic.object(forKey: "paths") as! NSDictionary
    let path = pathDictionary.object(forKey: "getFolio") as! String
    let url = "\(baseURL)\(path)/\(data)"
    let pncgrp = pncgrpDic.object(forKey: "picnic-group") as? String ?? ""
    let headder:HTTPHeaders = ["picnic-group":pncgrp]
    
    AF.request(url, method: .get, headers: headder){ urlRequest in
        urlRequest.timeoutInterval = timeoutInterval
    }.validate()
        .responseDecodable(of: FolioResultModel.self) { response in
        switch response.result {
        case .success:
            if let data = response.value{
                guard let folio = data.folioPedido else{
                    let statusCode = response.response?.statusCode ?? 0
                    let error = ErrorResponse(message: data.cMensaje ?? "", codeError: statusCode)
                    completion(nil, error)
                    return
                }
                completion(folio, nil)
            }
        case .failure(_):
            let statusCode = response.response?.statusCode ?? 0
            let error = ErrorResponse(message: "Error", codeError: statusCode)
            completion(nil ,error)

        }
    }
}
func createOrderRequest(data:CreateOrderModel, completion:@escaping(_ result:CreateOrderResponse?,_ error:ErrorResponse?) -> Void){

    guard let pncgrpDic = dictionary else {return}
    let baseURL = configuration.environment.baseURL
    let pathDictionary = pncgrpDic.object(forKey: "paths") as! NSDictionary
    let path = pathDictionary.object(forKey: "createOrder") as! String
    let url = "\(baseURL)\(path)"
    let pncgrp = pncgrpDic.object(forKey: "picnic-group") as? String ?? ""

    
    let headder:HTTPHeaders = ["picnic-group":pncgrp]
    AF.request(url, method: .post, parameters: data, encoder: JSONParameterEncoder.default, headers: headder){ urlRequest in
        urlRequest.timeoutInterval = timeoutInterval
    }.validate()
        .responseDecodable(of: CreateOrderResponse.self) { response in
        switch response.result {
        case .success:
            if let data = response.value{
                if data.bError {
                    let statusCode = response.response?.statusCode ?? 0
                    let error = ErrorResponse(message: "Error", codeError: statusCode)
                    completion(nil, error)
                }else{
                    completion(data, nil)
                }
            }
        case .failure(_):
            let statusCode = response.response?.statusCode ?? 0
            let error = ErrorResponse(message: "Error", codeError: statusCode)
            completion(nil,error)
        }
    }
    
}
