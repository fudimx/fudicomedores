//
//  BranchesRequestModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 22/04/22.
//

import Foundation
import Alamofire

private let timeoutInterval: Double = 10.0
private var configuration = Configuration()

private var dictionary:NSDictionary? {
    guard let path = Bundle.main.path(forResource: "Constants", ofType: "plist"), let dictionary = NSDictionary(contentsOfFile: path) else {
        return nil
    }
    return dictionary
}

func getBranchesRequest(idLocation:String, completion:@escaping(_ result:[BranchModel]) -> Void, failure:@escaping(_ error:ErrorResponse) -> Void){
    
    guard let pncgrpDic = dictionary else {return}
    let baseURL = configuration.environment.baseURL
    let pathDictionary = pncgrpDic.object(forKey: "paths") as! NSDictionary
    let path = pathDictionary.object(forKey: "getsucursales") as! String
    let url = "\(baseURL)\(path)/\(idLocation)"
    let pncgrp = pncgrpDic.object(forKey: "picnic-group") as? String ?? ""
    
    let headder:HTTPHeaders = ["picnic-group":pncgrp]
    AF.request(url, method: .get, headers: headder){ urlRequest in
        urlRequest.timeoutInterval = timeoutInterval
    }.validate()
        .responseDecodable(of: BaseArrayModel<BranchModel>.self) { response in
        switch response.result {
        case .success:
            if let data = response.value{
                if data.bError {
                    let statusCode = response.response?.statusCode ?? 0
                    let error = ErrorResponse(message: "Error", codeError: statusCode)
                    failure(error)
                }else{
                    completion(data.data ?? [])
                }
            }
        case .failure(_):
            let statusCode = response.response?.statusCode ?? 0
            let error = ErrorResponse(message: "Error", codeError: statusCode)
            failure(error)

        }
    }
}
