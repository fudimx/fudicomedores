//
//  CategoriesRequestModel.swift
//  FudiMX
//
//  Created by Cristian Canedo on 23/04/22.
//

import Foundation
import Alamofire

private let timeoutInterval: Double = 10.0
private var configuration = Configuration()

private var dictionary:NSDictionary? {
    guard let path = Bundle.main.path(forResource: "Constants", ofType: "plist"), let dictionary = NSDictionary(contentsOfFile: path) else {
        return nil
    }
    return dictionary
}
func getCargaInicial(idBranch:String, completion:@escaping(_ result:[MenuModel]) -> Void, failure:@escaping(_ error:ErrorResponse) -> Void){
    
    guard let pncgrpDic = dictionary else {return}
    let baseURL = configuration.environment.baseURL
    let pathDictionary = pncgrpDic.object(forKey: "paths") as! NSDictionary
    let path = pathDictionary.object(forKey: "getCargaInicial") as! String
    let url = "\(baseURL)\(path)/\(idBranch)"
    let pncgrp = pncgrpDic.object(forKey: "picnic-group") as? String ?? ""
    
    let headder:HTTPHeaders = ["picnic-group":pncgrp]
    
    AF.request(url, method: .get, headers: headder){ urlRequest in
        urlRequest.timeoutInterval = timeoutInterval
    }.validate()
        .responseDecodable(of: BaseArrayModel<CategoriesModel>.self) { response in
        switch response.result {
        case .success:
            if let data = response.value{
                if data.bError {
                    let statusCode = response.response?.statusCode ?? 0
                    let error = ErrorResponse(message: "Error", codeError: statusCode)
                    failure(error)
                }else{
                    guard let categories = data.data else {
                        completion([])
                        return
                    }
                    let emptyResult = CategoriesModel(lstCategorias: [], _id: "", nIdRestaurante: "")
                    let objcCategory = categories.isEmpty ? emptyResult:categories[0]
                    completion(objcCategory.lstCategorias)
                }
            }
        case .failure(_):
            let statusCode = response.response?.statusCode ?? 0
            let error = ErrorResponse(message: "Error", codeError: statusCode)
            failure(error)

        }
    }
}
